package com.mkm.publicservices.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.persistence.Entity;
import com.mkm.publicservices.model.AsignacionSubsidio;
import com.mkm.publicservices.model.CicloConsumo;
import com.mkm.publicservices.model.Consumo;
import com.mkm.publicservices.model.Direccion;
import com.mkm.publicservices.model.EstadoLiquidacion;
import com.mkm.publicservices.model.Hogar;
import com.mkm.publicservices.model.LiquidacionCicloVivienda;
import com.mkm.publicservices.model.LiquidacionValorConsumo;
import com.mkm.publicservices.model.Persona;
import com.mkm.publicservices.model.PromedioConsumoCiclo;
import com.mkm.publicservices.model.TipoConsumo;
import com.mkm.publicservices.model.TipoDocumento;
import com.mkm.publicservices.model.TipoEstadoLiquidacion;
import com.mkm.publicservices.model.TipoParametro;
import com.mkm.publicservices.model.TipoPerfil;
import com.mkm.publicservices.model.TipoVia;
import com.mkm.publicservices.model.Usuario;
import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.database.dao.CrudDao;
import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

/**
 * A factory for domain objects that mocks their data. Example usage:
 * <pre>
 *   <code>Blog blog = MockFactory.on(Blog.class).create(entityManager);</code>
 * </pre>
 * @author David Green
 */
public abstract class MockFactory<T> {

	private static Map<Class<?>, MockFactory<?>> factories = new HashMap<Class<?>, MockFactory<?>>();

	static {
		register(new MockAsignacionSubsidioFactory());
		register(new MockCicloConsumoFactory());
		register(new MockConsumoFactory());
		register(new MockDireccionFactory());
		register(new MockEstadoLiquidacionFactory());
		register(new MockHogarFactory());
		register(new MockLiquidacionCicloViviendaFactory());
		register(new MockLiquidacionValorConsumoFactory());
		register(new MockPersonaFactory());
		register(new MockPromedioConsumoCicloFactory());
		register(new MockTipoConsumoFactory());
		register(new MockTipoDocumentoFactory());
		register(new MockTipoEstadoLiquidacionFactory());
		register(new MockTipoParametroFactory());
		register(new MockTipoPerfilFactory());
		register(new MockTipoViaFactory());
		register(new MockUsuarioFactory());
		register(new MockValorConsumoFactory());
	}

	private static void register(MockFactory<?> mockFactory) {
		factories.put(mockFactory.domainClass, mockFactory);
	}

	@SuppressWarnings("unchecked")
	public static <T> MockFactory<T> on(Class<T> domainClass) {
		MockFactory<?> factory = factories.get(domainClass);
		if (factory == null) {
			throw new IllegalStateException("Did you forget to register a mock factory for " 
					+ domainClass.getClass().getName() + "?");
		}
		return (MockFactory<T>) factory;
	}

	private final Class<T> domainClass;

	private int seed;

	protected MockFactory(Class<T> domainClass) {
		if (domainClass.getAnnotation(Entity.class) == null) {
			throw new IllegalArgumentException();
		}
		this.domainClass = domainClass;
	}

	/**
	 * Create several objects
	 * @param entityManager the entity manager, or null if the mocked objects should not be persisted
	 * @param count the number of objects to create
	 * @return the created objects
	 */
	public List<T> create(int count) {
		List<T> mocks = new ArrayList<T>(count);
		for (int x = 0; x < count; ++x) {
			T t = create();
			mocks.add(t);
		}
		return mocks;
	}

	/**
	 * Create a single object
	 * @param entityManager the entity manager, or null if the mocked object should not be persisted
	 * @return the mocked object
	 */
	public T create() {
		T mock;
		try {
			mock = domainClass.newInstance();
		} catch (Exception e) {
			// must have a default constructor
			throw new IllegalStateException();
		}
		populate(++seed, mock);
		return create(mock);
	}

	/**
	 * Create a single object
	 * @param entityManager the entity manager, or null if the mocked object should not be persisted
	 * @return the mocked object
	 */
	public T create(T mock) {
		CrudDao<T> dao = new CrudDaoImpl<>(domainClass);
		dao.save(mock);
		return mock;
	}

	/**
	 * Populate the given domain object with data
	 * @param seed a seed that may be used to create data
	 * @param mock the domain object to populate
	 */
	protected abstract void populate(int seed, T mock);

	private static class MockAsignacionSubsidioFactory extends MockFactory<AsignacionSubsidio> {
		public MockAsignacionSubsidioFactory() {
			super(AsignacionSubsidio.class);
		}

		@Override
		protected void populate(int seed, AsignacionSubsidio mock) {
			mock.setHogar(MockFactory.on(Hogar.class).create(null));
			mock.setCicloConsumo(MockFactory.on(CicloConsumo.class).create(null));
		}
	}

	private static class MockCicloConsumoFactory extends MockFactory<CicloConsumo> {
		public MockCicloConsumoFactory() {
			super(CicloConsumo.class);
		}

		@Override
		protected void populate(int seed, CicloConsumo mock) {
			mock.setHogar(MockFactory.on(Hogar.class).create(null));
			mock.setFechaInicio(DateUtil.addDays(new Date(), -30));
			mock.setFechaFin(new Date());
		}
	}

	private static class MockConsumoFactory extends MockFactory<Consumo> {
		public MockConsumoFactory() {
			super(Consumo.class);
		}

		@Override
		protected void populate(int seed, Consumo mock) {
			mock.setTipoConsumo(MockFactory.on(TipoConsumo.class).create(null));
			mock.setConsumo(seed * 10000);
			mock.setCicloConsumo(MockFactory.on(CicloConsumo.class).create(null));
		}
	}

	private static class MockDireccionFactory extends MockFactory<Direccion> {
		public MockDireccionFactory() {
			super(Direccion.class);
		}

		@Override
		protected void populate(int seed, Direccion mock) {
			mock.setTipoVia(MockFactory.on(TipoVia.class).create(null));
			mock.setNumeroViaPrincipal(seed);
			mock.setNumeroViaGeneradora(seed);
			mock.setComplemento(seed);
		}
	}

	private static class MockEstadoLiquidacionFactory extends MockFactory<EstadoLiquidacion> {
		public MockEstadoLiquidacionFactory() {
			super(EstadoLiquidacion.class);
		}

		@Override
		protected void populate(int seed, EstadoLiquidacion mock) {
			mock.setLiquidacionCicloVivienda(MockFactory.on(LiquidacionCicloVivienda.class).create(null));
			mock.setTipoEstadoLiquidacion(MockFactory.on(TipoEstadoLiquidacion.class).create(null));
			mock.setFechaRegistro(Timestamp.from(new Date().toInstant()));
			mock.setUsuario(MockFactory.on(Usuario.class).create(null));
		}
	}

	private static class MockHogarFactory extends MockFactory<Hogar> {
		public MockHogarFactory() {
			super(Hogar.class);
		}

		@Override
		protected void populate(int seed, Hogar mock) {
			mock.setDireccion(MockFactory.on(Direccion.class).create(null));
			mock.setTelefono(PhoneGenerator.generate());
			mock.setResponsable(MockFactory.on(Persona.class).create(null));
			mock.setEstratoSocioeconomico(new Random().nextInt(5+1));
			mock.setUsuario(MockFactory.on(Usuario.class).create(null));
		}
	}

	private static class MockLiquidacionCicloViviendaFactory extends MockFactory<LiquidacionCicloVivienda> {
		public MockLiquidacionCicloViviendaFactory() {
			super(LiquidacionCicloVivienda.class);
		}

		@Override
		protected void populate(int seed, LiquidacionCicloVivienda mock) {
			mock.setHogar(MockFactory.on(Hogar.class).create(null));
			mock.setCicloConsumo(MockFactory.on(CicloConsumo.class).create(null));
		}
	}

	private static class MockLiquidacionValorConsumoFactory extends MockFactory<LiquidacionValorConsumo> {
		public MockLiquidacionValorConsumoFactory() {
			super(LiquidacionValorConsumo.class);
		}

		@Override
		protected void populate(int seed, LiquidacionValorConsumo mock) {
			mock.setLiquidacionCicloVivienda(MockFactory.on(LiquidacionCicloVivienda.class).create(null));
			mock.setConsumo(MockFactory.on(Consumo.class).create(null));
			mock.setValorConsumo(BigDecimal.valueOf(seed * 100000));
			mock.setAplicaSubsidio((byte) new Random().nextInt(2));
		}
	}

	private static class MockPersonaFactory extends MockFactory<Persona> {
		public MockPersonaFactory() {
			super(Persona.class);
		}

		@Override
		protected void populate(int seed, Persona mock) {
			mock.setTipoDocumento(MockFactory.on(TipoDocumento.class).create(null));
			mock.setNumeroDocumento(String.valueOf(new Random().nextInt(89999999 + 10000000)));
			mock.setNombre("Nombre " + seed);
			mock.setApellido("Apellido " + seed);
			mock.setFechaNacimiento(DateUtil.addDays(new Date(), -23413));
			mock.setEmail(String.format("email%s@mail.com", seed));
			mock.setGenero((byte) new Random().nextInt(2));
			mock.setFechaRegistro(Timestamp.from(new Date().toInstant()));
		}
	}

	private static class MockPromedioConsumoCicloFactory extends MockFactory<PromedioConsumoCiclo> {
		public MockPromedioConsumoCicloFactory() {
			super(PromedioConsumoCiclo.class);
		}

		@Override
		protected void populate(int seed, PromedioConsumoCiclo mock) {
			mock.setTipoConsumo(MockFactory.on(TipoConsumo.class).create(null));
			mock.setPromedio(25);
			mock.setAsignacionSubsidio(MockFactory.on(AsignacionSubsidio.class).create(null));
		}
	}

	private static class MockTipoConsumoFactory extends MockFactory<TipoConsumo> {
		public MockTipoConsumoFactory() {
			super(TipoConsumo.class);
		}

		@Override
		protected void populate(int seed, TipoConsumo mock) {
			mock.setTipoConsumo("Tipo de consumo " + seed);
		}
	}

	private static class MockTipoDocumentoFactory extends MockFactory<TipoDocumento> {
		public MockTipoDocumentoFactory() {
			super(TipoDocumento.class);
		}

		@Override
		protected void populate(int seed, TipoDocumento mock) {
			mock.setTipoDocumento("Tipo de documento " + seed);
			mock.setAbreviacion("Abrev " + seed);
		}
	}

	private static class MockTipoEstadoLiquidacionFactory extends MockFactory<TipoEstadoLiquidacion> {
		public MockTipoEstadoLiquidacionFactory() {
			super(TipoEstadoLiquidacion.class);
		}

		@Override
		protected void populate(int seed, TipoEstadoLiquidacion mock) {
			mock.setTipoEstadoLiquidacion("Tipo estado liquidación " + seed);
		}
	}

	private static class MockTipoParametroFactory extends MockFactory<TipoParametro> {
		public MockTipoParametroFactory() {
			super(TipoParametro.class);
		}

		@Override
		protected void populate(int seed, TipoParametro mock) {
			mock.setTipoParametro("Tipo parámetro " + seed);
			mock.setValorParametro("Valor " + seed);
			mock.setDescripcion("Descripción " + seed);
		}
	}

	private static class MockTipoPerfilFactory extends MockFactory<TipoPerfil> {
		public MockTipoPerfilFactory() {
			super(TipoPerfil.class);
		}

		@Override
		protected void populate(int seed, TipoPerfil mock) {
			mock.setTipoPerfil("Tipo perfil "+ seed);
		}
	}

	private static class MockTipoViaFactory extends MockFactory<TipoVia> {
		public MockTipoViaFactory() {
			super(TipoVia.class);
		}

		@Override
		protected void populate(int seed, TipoVia mock) {
			mock.setTipoVia("Tipo vía " + seed);
			mock.setAbreviacion("Abrev " + seed);
		}
	}

	private static class MockUsuarioFactory extends MockFactory<Usuario> {
		public MockUsuarioFactory() {
			super(Usuario.class);
		}

		@Override
		protected void populate(int seed, Usuario mock) {
			mock.setPersona(MockFactory.on(Persona.class).create(null));
			mock.setNombreUsuario("username" + seed);
			mock.setContrasena("contrasena" + seed);
			mock.setEstado((byte) 1);
			mock.setFechaCreacion(Timestamp.from(new Date().toInstant()));
		}
	}

	private static class MockValorConsumoFactory extends MockFactory<ValorConsumo> {
		public MockValorConsumoFactory() {
			super(ValorConsumo.class);
		}

		@Override
		protected void populate(int seed, ValorConsumo mock) {
			mock.setTipoConsumo(MockFactory.on(TipoConsumo.class).create(null));
			mock.setValorConsumo(BigDecimal.valueOf(seed));
			mock.setAnno(new Date());
		}
	}
}