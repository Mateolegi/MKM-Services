package com.mkm.publicservices.util;

import java.util.Random;

public class PhoneGenerator {

	public static String generate() {
		int set2, set3; // sequence 2 and 3 of the phone number
		Random generator = new Random();
		// Sequence two of phone number
		// the plus 100 is so there will always be a 3 digit number
		// randomize to 643 because 0 starts the first placement so if i randomized up
		// to 642 it would only go up yo 641 plus 100
		// and i used 643 so when it adds 100 it will not succeed 742
		set2 = generator.nextInt(643) + 100;
		// Sequence 3 of numebr
		// add 1000 so there will always be 4 numbers
		// 8999 so it wont succed 9999 when the 1000 is added
		set3 = generator.nextInt(8999) + 1000;
		return "+57 " + set2 + " " + set3;
	}
}
