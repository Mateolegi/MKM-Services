package com.mkm.publicservices.model;

import static org.junit.Assert.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.junit.Test;

import com.mkm.publicservices.util.AssertAnnotations;
import com.mkm.publicservices.util.ReflectTool;

public class AsignacionSubsidioTest {

	@Test
	public void typeAnnotations() {
		// assert
		AssertAnnotations.assertType(AsignacionSubsidio.class, Entity.class, Table.class, NamedQuery.class);
	}

	@Test
	public void fieldAnnotations() {
		// assert
		AssertAnnotations.assertField(AsignacionSubsidio.class, "idAsignacionSubsidio", Id.class, GeneratedValue.class, 
				Column.class);
		AssertAnnotations.assertField(AsignacionSubsidio.class, "cicloConsumo", ManyToOne.class, JoinColumn.class);
		AssertAnnotations.assertField(AsignacionSubsidio.class, "hogar", ManyToOne.class, JoinColumn.class);
		AssertAnnotations.assertField(AsignacionSubsidio.class, "promedioConsumoCiclos", OneToMany.class);
	}

	@Test
	public void methodAnnotations() {
		// assert
		AssertAnnotations.assertMethod(AsignacionSubsidio.class, "getIdAsignacionSubsidio");
		AssertAnnotations.assertMethod(AsignacionSubsidio.class, "getCicloConsumo");
		AssertAnnotations.assertMethod(AsignacionSubsidio.class, "getHogar");
		AssertAnnotations.assertMethod(AsignacionSubsidio.class, "getPromedioConsumoCiclos");
	}

	@Test
	public void entity() {
		// setup
		Entity a = ReflectTool.getClassAnnotation(AsignacionSubsidio.class, Entity.class);
		// assert
		assertEquals("", a.name());
	}

	@Test
	public void table() {
		// setup
		Table t = ReflectTool.getClassAnnotation(AsignacionSubsidio.class, Table.class);
		// assert
		assertEquals("asignacion_subsidio", t.name());
	}

	@Test
	public void idAsignacionSubsidio() {
		// setup
		GeneratedValue a = ReflectTool.getFieldAnnotation(AsignacionSubsidio.class, "idAsignacionSubsidio", GeneratedValue.class);
		// assert
		assertEquals("", a.generator());
		assertEquals(GenerationType.IDENTITY, a.strategy());
	}

	@Test
	public void cicloConsumo() {
		// setup
		JoinColumn c = ReflectTool.getFieldAnnotation(AsignacionSubsidio.class, "cicloConsumo", JoinColumn.class);
		// assert
		assertEquals("ID_CICLO_CONSUMO", c.name());
	}

	@Test
	public void hogar() {
		// setup
		JoinColumn c = ReflectTool.getFieldAnnotation(AsignacionSubsidio.class, "hogar", JoinColumn.class);
		// assert
		assertEquals("ID_HOGAR", c.name());
	}

	@Test
	public void promedioConsumoCiclos() {
		// setup
		OneToMany a = ReflectTool.getFieldAnnotation(AsignacionSubsidio.class, "promedioConsumoCiclos", OneToMany.class);
		// assert
		assertEquals("asignacionSubsidio", a.mappedBy());
		assertEquals(FetchType.LAZY, a.fetch());
	}
}
