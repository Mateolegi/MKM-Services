package com.mkm.publicservices.model;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.mkm.publicservices.util.MockFactory;

import io.github.mateolegi.util.HibernateUtil;
import io.github.mateolegi.util.TransactionException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TipoDocumentoTest {

	private static final Logger LOGGER = LogManager.getLogger(TipoDocumentoTest.class);
	private EntityManager entityManager = HibernateUtil.createEntityManager();

	@Test
	public void A_testSave() {
		try {
			TipoDocumento tipoDocumento = new TipoDocumento();
			tipoDocumento.setTipoDocumento("Cédula de ciudadanía");
			tipoDocumento.setAbreviacion("CC");
			tipoDocumento = MockFactory.on(TipoDocumento.class).create(tipoDocumento);
			assertNotNull(tipoDocumento);
			assertNotNull(tipoDocumento.getTipoDocumento());
			tipoDocumento = new TipoDocumento();
			tipoDocumento.setTipoDocumento("Cédula de extranjería");
			tipoDocumento.setAbreviacion("CE");
			tipoDocumento = MockFactory.on(TipoDocumento.class).create(tipoDocumento);
			assertNotNull(tipoDocumento);
			assertNotNull(tipoDocumento.getTipoDocumento());
		} catch (TransactionException e) {
			LOGGER.error(e);
			fail("Ocurrió un error guardando una de las entidades");
		}
	}

	@Test
	public void B_testFind() {
		TipoDocumento tipoDocumento = entityManager.find(TipoDocumento.class, 0);
		assertEquals(0, tipoDocumento.getIdTipoDocumento());
	}

	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	 /**
	  * Overriding methods should call super.
	  */ 
	@After
	public void after() {
		if (entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().rollback();
		}
	}
}
