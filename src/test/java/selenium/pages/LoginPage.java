package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	public WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement user() {
		return driver.findElement(By.id("usuario"));
	}
	
	public WebElement password() {
		return driver.findElement(By.id("contrasena"));
	}
	
	public WebElement botonInicio() {
		return driver.findElement(By.xpath("//*[@id=\"formLogin\"]/div/div[3]/div/input"));
	}
}
