package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class NuevoResponsablePage {
public static WebElement element;
public static Select seleccion;
	
	public static WebElement nuevoResponsable (WebDriver driver) {
		element=driver.findElement(By.xpath("//*[@id=\"seleccionResponsable\"]/div/div/div[2]/div[1]/div[2]/label"));
		
		
		return element;
	}
	
	public static Select tipoDocumento (WebDriver driver) {
		seleccion = new Select (driver.findElement(By.id("tipo_documento")));
		
		return seleccion;
	}
	
	public static WebElement numeroDocumento (WebDriver driver) {
		element=driver.findElement(By.id("numeroDocumento"));
		return element;
	}
	
	public static WebElement nombre (WebDriver driver) {
		element=driver.findElement(By.id("nombre"));
		return element;
	}
	
	public static WebElement apellido (WebDriver driver) {
		element=driver.findElement(By.id("apellido"));
		return element;
	}
	
	public static WebElement correoElectronico (WebDriver driver) {
		element=driver.findElement(By.id("email"));
		return element;
	}
	
	public static WebElement fechaNacimiento (WebDriver driver) {
		element=driver.findElement(By.id("fechaNacimiento"));
		return element;
	}
	
	public static Select genero (WebDriver driver) {
		seleccion = new Select (driver.findElement(By.id("genero")));
		
		return seleccion;
	}
	
	public static WebElement botonCrear (WebDriver driver) {
		element=driver.findElement(By.xpath("//*[@id=\"seleccionResponsable\"]/div/div/div[3]/input"));
		return element;
	}
	
	

}
