package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UsuariosPage {
	
	private static WebElement element;
	
	public static WebElement nuevoUsuario(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@class='btn btn-secondary btn-sm']"));
		return element;
	}
	
	public static WebElement editarUsuario(WebDriver driver) {
		element = driver.findElement(By.xpath("//html/body/app-root/div/div/main/div/app-main/app-usuario/div/div/div/div/div[2]/table/tbody/tr[1]/td[5]/button[1]"));
		return element;
	}
	
	public static WebElement eliminarUsuario(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@class='btn btn-danger btn-sm']"));
		return  element;
	}

	
	
}
