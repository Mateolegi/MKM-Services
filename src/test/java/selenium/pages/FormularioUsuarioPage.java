package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FormularioUsuarioPage {
	
	private static WebElement element;
	private static Select seleccion;
	
	public static WebElement nombreUsuario (WebDriver driver) {
		element = driver.findElement(By.id("nombre_usuario"));
		return element;
	}

	public static Select perfil (WebDriver driver) {
		seleccion = new Select( driver.findElement(By.id("tipo_perfil")));
		return seleccion;
	}
	
	public static Select tipoDocumento (WebDriver driver) {
		seleccion = new Select (driver.findElement(By.id("tipo_documento")));
		return seleccion;
	}
	
	public static WebElement numeroDocumento (WebDriver driver) {
		element = driver.findElement(By.id("numero_documento"));
		return element;
	}
	
	public static WebElement nombre(WebDriver driver) {
		element = driver.findElement(By.id("nombre"));
		return element;
	}
	
	public static WebElement apellido (WebDriver driver) {
		element = driver.findElement(By.id("apellido"));
		return element;
		
	}
	
	public static WebElement correo(WebDriver driver) {
		element = driver.findElement(By.id("email"));
		return element;
		
	}
	
	public static WebElement fechaNacimiento (WebDriver driver) {
		element = driver.findElement(By.id("fechaNacimiento"));
		return element;
	}
	
	public static Select genero(WebDriver driver) {
		seleccion = new Select(driver.findElement(By.id("genero")));
		return seleccion;
	}
	
	public static WebElement botonRegistrar (WebDriver driver) {
		element = driver.findElement(By.xpath("//button[text()='Registrar']"));
		return element;
	}
	public static WebElement botonActualizar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[text()='Actualizar']"));
		return element;
	}
	
}
