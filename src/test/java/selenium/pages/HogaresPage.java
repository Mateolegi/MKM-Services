package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HogaresPage {
	public static WebElement element;
	

public static WebElement botonNuevo(WebDriver driver) {
	element = driver.findElement(By.xpath("/html/body/app-root/div/div/main/div/app-main/app-hogar/div/div/div/div/div[1]/div/button"));
	return element;
}
	
	public static WebElement botonEditar(WebDriver driver) {
		element = driver.findElement(By.xpath("/html/body/app-root/div/div/main/div/app-main/app-hogar/div/div/div/div/div[2]/table/tbody/tr/td[5]/button[1]"));
		return element;
	}
	
	public static WebElement botonEliminar(WebDriver driver) {
		element = driver.findElement(By.xpath("/html/body/app-root/div/div/main/div/app-main/app-hogar/div/div/div/div/div[2]/table/tbody/tr/td[5]/button[2]"));
		return element;
	}
	
	public static WebElement irAdministracion(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[text()='Administración']"));
		return element;
	}
	
	public static WebElement irUsuarios(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@class='nav-link']//i[@class='nav-icon icon-user']"));
		return element;
	}
	
	

}
