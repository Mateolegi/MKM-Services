package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HogarDetallePage {
	
	public static WebElement element;
	public static Select seleccion;
	
	public static WebElement responsable (WebDriver driver) {
		element=driver.findElement(By.xpath("//*[@id=\"formHogar\"]/div[1]/div[1]/div/div/div"));
		return element;
	}
	
	public static WebElement telefono (WebDriver driver) {
		element = driver.findElement(By.id("phone"));
		return element;
	}
	
	public static WebElement estrato (WebDriver driver) {
		element=driver.findElement(By.id("estrato"));
		return element;
	}
	
	public static Select tipoVia (WebDriver driver) {
		seleccion = new Select (driver.findElement(By.id("tipo_via")));
		
		return seleccion;
		
	}
	
	
	public static WebElement viaPrincipal (WebDriver driver) {
		element = driver.findElement(By.id("via_principal"));
		return element;
	}
	
	//Select para poder escoger de la lista desplegable
	public static Select letraPrincipal (WebDriver driver) {
		
		seleccion = new Select (driver.findElement(By.id("letra_principal")));
		
		return seleccion;
	}
	
	public static Select cuadrantePrincipal (WebDriver driver) {
		
seleccion = new Select (driver.findElement(By.id("cuadrante-principal")));
		
		return seleccion;
		
		
	}
	
	public static WebElement generadora (WebDriver driver) {
		element = driver.findElement(By.id("via_generadora"));
		return element;
	}
	
	public static Select letraGeneradora (WebDriver driver) {
		;
seleccion = new Select (driver.findElement(By.id("letra_generadora")));
		return seleccion;
	}

	public static WebElement complemento (WebDriver driver) {
		element = driver.findElement(By.id("complemento"));
		return element;
	}
	
	public static WebElement interior (WebDriver driver) {
		element = driver.findElement(By.id("interior"));
		return element;
	}
	public static WebElement botonRegistrar (WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"formHogar\"]/div[5]/div/div/button[1]"));
		return element;
	}
	
	public static WebElement botonActualizar (WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"formHogar\"]/div[5]/div/div/button[1]"));
		return element;
	}
}
