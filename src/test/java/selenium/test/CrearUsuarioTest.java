package selenium.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import selenium.pages.HogaresPage;
import selenium.pages.LoginPage;
import selenium.pages.FormularioUsuarioPage;
import selenium.pages.UsuariosPage;

public class CrearUsuarioTest {
	private static WebDriver driver;
	private LoginPage loginPage;

	@BeforeClass
	public static void createAndStartService() throws IOException {
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Before
	public void createDriver() {
		loginPage = new LoginPage(driver);
	}

	private void login() {
		loginPage.user().sendKeys("mateolegi");
		assertEquals(loginPage.user().getAttribute("ng-reflect-model"), "mateolegi");
		loginPage.password().sendKeys("admin");
		assertEquals(loginPage.password().getAttribute("ng-reflect-model"), "admin");
		loginPage.botonInicio().click();
		esperarSegundos(driver, 4);
	}

	@Test
	public void crearNuevoUsuario() {

		driver.get("http://localhost:4200/hogares");
		driver.manage().window().maximize();
		login();
		HogaresPage.irAdministracion(driver).click();
		HogaresPage.irUsuarios(driver).click();

		UsuariosPage.nuevoUsuario(driver).click();
		FormularioUsuarioPage.nombreUsuario(driver).sendKeys("Kate");
		FormularioUsuarioPage.perfil(driver).selectByIndex(1);
		FormularioUsuarioPage.tipoDocumento(driver).selectByIndex(1);
		FormularioUsuarioPage.numeroDocumento(driver).sendKeys("12345321");
		FormularioUsuarioPage.nombre(driver).sendKeys("kateryn");
		FormularioUsuarioPage.apellido(driver).sendKeys("Bustamante");
		FormularioUsuarioPage.correo(driver).sendKeys("ky@gmail.com");
		FormularioUsuarioPage.fechaNacimiento(driver).sendKeys("12021999");
		FormularioUsuarioPage.genero(driver).selectByVisibleText("Femenino");
		esperarSegundos(driver, 3);
		FormularioUsuarioPage.botonRegistrar(driver).click();
	}

	@After
	public void quitDriver() {
		driver.quit();
	}

	public void esperarSegundos(WebDriver driver, int segundos) {

		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
