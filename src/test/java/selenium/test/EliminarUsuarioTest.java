package selenium.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import selenium.pages.HogaresPage;
import selenium.pages.LoginPage;
import selenium.pages.UsuariosPage;

public class EliminarUsuarioTest {
	private static WebDriver driver;
	private LoginPage loginPage;

	@BeforeClass
	public static void createAndStartService() throws IOException {
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Before
	public void createDriver() {
		loginPage = new LoginPage(driver);
	}

	private void login() {
		loginPage.user().sendKeys("mateolegi");
		assertEquals(loginPage.user().getAttribute("ng-reflect-model"), "mateolegi");
		loginPage.password().sendKeys("admin");
		assertEquals(loginPage.password().getAttribute("ng-reflect-model"), "admin");
		loginPage.botonInicio().click();
		esperarSegundos(driver, 4);
	}

	@Test
	public void crearNuevoUsuario() {

		driver.get("http://localhost:4200/hogares");
		driver.manage().window().maximize();
		login();
		HogaresPage.irAdministracion(driver).click();
		HogaresPage.irUsuarios(driver).click();
		UsuariosPage.eliminarUsuario(driver).click();

		esperarSegundos(driver, 3);

	}

	@After
	public void quitDriver() {
		driver.quit();
	}

	public void esperarSegundos(WebDriver driver, int segundos) {

		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
