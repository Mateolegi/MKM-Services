package selenium.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import selenium.pages.*;

public class LoginTest {
	private static WebDriver driver = null;
	private static LoginPage loginPage;

	@BeforeClass
	public static void createAndStartService() throws IOException {
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Before
	public void createDriver() {
		loginPage = new LoginPage(driver);
	}

	@Test
	public void inicioSesionCorrecto() {

		driver.get("http://localhost:4200/login");
		driver.manage().window().maximize();

		loginPage.user().sendKeys("mateolegi");
		assertEquals(loginPage.user().getAttribute("ng-reflect-model"), "mateolegi");
		loginPage.password().sendKeys("admin");
		assertEquals(loginPage.password().getAttribute("ng-reflect-model"), "admin");
		loginPage.botonInicio().click();
		esperarSegundos(driver, 2);
	}

	@Test
	public void inicioSesionIncorrecto() {
		driver.get("http://localhost:4200/login");
		driver.manage().window().maximize();
		loginPage.user().sendKeys("mateolegi");
		assertEquals(loginPage.user().getAttribute("ng-reflect-model"), "mateolegi");
		loginPage.botonInicio().click();
		esperarSegundos(driver, 2);
	}

	@After
	public void quitDriver() {
		driver.quit();
	}

	public void esperarSegundos(WebDriver driver, int segundos) {
		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				// ignore
			}
		}
	}
}
