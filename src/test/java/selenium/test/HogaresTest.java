package selenium.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import selenium.pages.*;

public class HogaresTest {
	private static WebDriver driver;
	private LoginPage loginPage;

	@BeforeClass
	public static void createAndStartService() throws IOException {
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Before
	public void createDriver() {
		loginPage = new LoginPage(driver);
	}

	private void login() {
		loginPage.user().sendKeys("mateolegi");
		assertEquals(loginPage.user().getAttribute("ng-reflect-model"), "mateolegi");
		loginPage.password().sendKeys("admin");
		assertEquals(loginPage.password().getAttribute("ng-reflect-model"), "admin");
		loginPage.botonInicio().click();
		esperarSegundos(driver, 4);
	}

	@Test
	public void nuevoHogar() {
		driver.get("http://localhost:4200/login");
		login();
		HogaresPage.botonNuevo(driver).click();
		HogarDetallePage.responsable(driver).click();
		esperarSegundos(driver, 2);
		NuevoResponsablePage.nuevoResponsable(driver).click();
		NuevoResponsablePage.tipoDocumento(driver).selectByIndex(1);// Se selecciona el item de la lista desplegable por
																	// medio del index
		NuevoResponsablePage.numeroDocumento(driver).sendKeys("896513541653");
		assertEquals(NuevoResponsablePage.numeroDocumento(driver).getAttribute("ng-reflect-model"), "896513541653");
		NuevoResponsablePage.nombre(driver).sendKeys("Andrés");
		assertEquals(NuevoResponsablePage.nombre(driver).getAttribute("ng-reflect-model"), "Andrés");
		NuevoResponsablePage.apellido(driver).sendKeys("Casas");
		assertEquals(NuevoResponsablePage.apellido(driver).getAttribute("ng-reflect-model"), "Casas");
		NuevoResponsablePage.correoElectronico(driver).sendKeys("andrescoronel@gmail.com");
		assertEquals(NuevoResponsablePage.correoElectronico(driver).getAttribute("ng-reflect-model"), "andrescoronel@gmail.com");
		NuevoResponsablePage.fechaNacimiento(driver).sendKeys("27091920");
		assertEquals(NuevoResponsablePage.fechaNacimiento(driver).getAttribute("ng-reflect-model"), "27091920");
		NuevoResponsablePage.fechaNacimiento(driver).sendKeys(Keys.TAB);
		NuevoResponsablePage.genero(driver).selectByVisibleText("Masculino");
		NuevoResponsablePage.botonCrear(driver).click();
		esperarSegundos(driver, 2);

		HogarDetallePage.telefono(driver).sendKeys("3331234354");
		assertEquals(HogarDetallePage.telefono(driver).getAttribute("ng-reflect-model"), "27091920");
		HogarDetallePage.estrato(driver).sendKeys("3");
		assertEquals(HogarDetallePage.estrato(driver).getAttribute("ng-reflect-model"), "3");
		HogarDetallePage.tipoVia(driver).selectByVisibleText("Calle");// y aqui por medio del texto visible
		HogarDetallePage.viaPrincipal(driver).sendKeys("78");
		assertEquals(HogarDetallePage.viaPrincipal(driver).getAttribute("ng-reflect-model"), "78");
		HogarDetallePage.letraPrincipal(driver).selectByVisibleText("B");
		HogarDetallePage.cuadrantePrincipal(driver).selectByVisibleText("Sur");
		HogarDetallePage.generadora(driver).sendKeys("45");
		assertEquals(HogarDetallePage.generadora(driver).getAttribute("ng-reflect-model"), "45");
		HogarDetallePage.letraGeneradora(driver).selectByVisibleText("C");
		;
		HogarDetallePage.complemento(driver).sendKeys("32");
		HogarDetallePage.interior(driver).sendKeys("205");
		HogarDetallePage.botonRegistrar(driver).click();
		esperarSegundos(driver, 2);

	}

	@Test
	public void editarHogar() {
		driver.get("http://localhost:4200/login");
		driver.manage().window().maximize();
		login();
		HogaresPage.botonEditar(driver).click();
		HogarDetallePage.telefono(driver).clear();
		HogarDetallePage.telefono(driver).sendKeys("2133687");
		assertEquals(HogarDetallePage.telefono(driver).getAttribute("ng-reflect-model"), "2133687");
		HogarDetallePage.botonActualizar(driver).click();

		esperarSegundos(driver, 2);

	}

	@After
	public void quitDriver() {
		driver.quit();
	}

	public void esperarSegundos(WebDriver driver, int segundos) {

		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
