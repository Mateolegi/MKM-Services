/*
 * The MIT License
 * 
 * Copyright (c) 2010-2018 Google, Inc. http://angularjs.org
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS I
 * THE SOFTWARE.
 */
package io.github.mateolegi.util;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Clase de utilidades para el control de la fábrica de sesiones
 * @author <a href="https://mateolegi.github.io">Mateo Leal</a>
 */
@WebListener
public class HibernateUtil implements ServletContextListener {

	private static EntityManagerFactory entityManagerFactory;
	private static final Logger LOGGER = LogManager.getLogger(HibernateUtil.class);

	private HibernateUtil() { }

	private static synchronized void buildEntityManagerFactory() {
		if (entityManagerFactory == null) {
			if (isJUnitTest()) {
				entityManagerFactory = Persistence.createEntityManagerFactory("public-services-test");
			} else {
				entityManagerFactory = Persistence.createEntityManagerFactory("public-services");
			}
		}
	}

	public static EntityManager createEntityManager() {
		if (entityManagerFactory == null) {
            throw new IllegalStateException("Context is not initialized yet.");
        }
		return entityManagerFactory.createEntityManager();
	}

	private static void closeEntityManagerFactory() {
		if (entityManagerFactory != null) {
			entityManagerFactory.close();
		}
	}

	public static boolean isJUnitTest() {
	    StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	    List<StackTraceElement> list = Arrays.asList(stackTrace);
	    for (StackTraceElement element : list) {
	        if (element.getClassName().startsWith("org.junit.")) {
	        	LOGGER.info("Está corriendo una prueba en JUnit");
	            return true;
	        }           
	    }
	    return false;
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		closeEntityManagerFactory();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		buildEntityManagerFactory();
	}
}
