package io.github.mateolegi.manager.impl;

import java.lang.reflect.Field;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MultivaluedMap;

import io.github.mateolegi.database.dao.CrudDao;
import io.github.mateolegi.manager.CrudManager;

public class CrudManagerImpl<E> implements CrudManager<E> {
	
	/** Clase que representa la entidad que se mapea en las consultas */
	private final CrudDao<E> dao;

	public CrudManagerImpl(CrudDao<E> dao) {
		this.dao = dao;
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.manager.Crud#findAll()
	 */
	@Override
	public List<E> findAll(MultivaluedMap<String, String> queryParams) {
		return dao.findAll(queryParams);
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.manager.Crud#find(java.lang.Object)
	 */
	@Override
	public E find(Object id) {
		return dao.find(id);
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.manager.Crud#save(java.lang.Object)
	 */
	@Override
	public E save(E entity) {
		return dao.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.manager.Crud#update(java.lang.Object)
	 */
	@Override
	public E update(int id, E entity) {
		if (id == getIdValue(entity)) {
			return dao.update(entity);
		}
		throw new BadRequestException("El identificador no coincide");
	}

	private int getIdValue(E entity) {
		Field[] fields = entity.getClass().getDeclaredFields();
		for (Field field: fields) {
			String className = entity.getClass().getSimpleName().toUpperCase();
			if (field.getName().toUpperCase().contains("ID") 
					&& field.getName().toUpperCase().contains(className)) {
				field.setAccessible(true);
				try {
					return (int) field.get(entity);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					//
				}
			}
		}
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.manager.Crud#delete(java.lang.Object)
	 */
	@Override
	public void delete(int id) {
		E entity = this.find(id);
		if (entity != null) {
			dao.delete(entity);
		}
		throw new BadRequestException("No existe una entidad con ese identificador");
	}
}
