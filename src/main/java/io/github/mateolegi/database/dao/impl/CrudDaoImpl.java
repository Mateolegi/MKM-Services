/*
 * The MIT License
 * 
 * Copyright (c) 2010-2018 Google, Inc. http://angularjs.org
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS I
 * THE SOFTWARE.
 */
package io.github.mateolegi.database.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.github.mateolegi.database.dao.CrudDao;
import io.github.mateolegi.util.HibernateUtil;
import io.github.mateolegi.util.TransactionException;

/**
 * Implementación de la capa de acceso a datos genérica
 * @author <a href="https://mateolegi.github.io">Mateo Leal</a>
 * @param <E> Clase de la entidad
 */
public class CrudDaoImpl<E> implements CrudDao<E> {

	/** Clase que representa la entidad que se mapea en las consultas */
	private final Class<E> clazz;

	protected EntityManager entityManager;
	private static final Logger LOGGER = LogManager.getLogger(CrudDaoImpl.class);

	public CrudDaoImpl(Class<E> clazz) {
		this.clazz = clazz;
	}

	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		try {
			entityManager = HibernateUtil.createEntityManager();
			Query query = entityManager.createNamedQuery(String.format("%s.findAll", clazz.getSimpleName()));
			return (List<E>) query.getResultList();
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud#findAll()
	 */
	@Override
	public List<E> findAll(MultivaluedMap<String, String> queryParams) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			LOGGER.debug("Inicia una transacción");
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<E> query = builder.createQuery(clazz);
			Root<E> root = query.from(clazz);
			query.select(root);
			// Obtiene los filtros de búsqueda que se enviaron al servicio
			List<Predicate> predicates = new ArrayList<>();
			if (queryParams != null) {
				queryParams.forEach((k, v) -> {
					try {
						Predicate predicate = builder.equal(root.get(k), v);
						predicates.add(predicate);
						LOGGER.debug(String.format("Se agrega el filtro para %s = %s", k, v));
					} catch (Exception e) {
						LOGGER.warn(String.format("No se encontró el atributo %s", k), e);
					}
				});
			}
			// Si se ingresaron filtros de búsqueda se aplican a la consulta
			if (!predicates.isEmpty()) {
				query.where(predicates.toArray(new Predicate[predicates.size()]));
			}
			TypedQuery<E> q = entityManager.createQuery(query);
			List<E> list = q.getResultList();
			LOGGER.debug("Finaliza la transacción");
			return list;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud#find(int)
	 */
	@Override
	public E find(Object id) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			E element = entityManager.find(clazz, id);
			return element;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud#save(java.lang.Object)
	 */
	@Override
	public E save(E entity) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.flush();
			entityManager.getTransaction().commit();
			return entity;
		} catch (RuntimeException e) {
			handleError();
			//LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			handleError();
			//LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud#update(java.lang.Object)
	 */
	@Override
	public E update(E entity) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			entityManager.getTransaction().begin();
			E updatedEntity = entityManager.merge(entity);
			entityManager.getTransaction().commit();
			return updatedEntity;
		} catch (RuntimeException e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud#delete(java.lang.Object)
	 */
	@Override
	public void delete(E entity) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			entityManager.getTransaction().begin();
			entityManager.remove(entity);
			entityManager.getTransaction().commit();
		} catch (RuntimeException e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.Crud
	 * #nativeQuery(java.lang.String, boolean, java.lang.String[])
	 */
	@Override
	public Object nativeQuery(String sql, boolean isList, Object... values) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			entityManager.getTransaction().begin();
			Query query = entityManager.createNativeQuery(sql);
			if (values != null) {
				int index = 1;
				for (Object value: values) {
					query.setParameter(index++, value);
				}
			}
			entityManager.getTransaction().commit();
			return isList ? query.getResultList() : query.getSingleResult();
		} catch (NoResultException e) {
			return isList ? Collections.emptyList() : null;
		} catch (RuntimeException e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			handleError();
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see io.github.mateolegi.database.dao.CrudDao
	 * #jpqlQuery(java.lang.String, boolean, java.lang.String[])
	 */
	@Override
	public Object jpqlQuery(String jpql, boolean isList, Object... values) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			TypedQuery<E> query = entityManager.createQuery(jpql, clazz);
			if (values != null) {
				int index = 1;
				for (Object value: values) {
					query.setParameter(index++, value);
				}
			}
			return isList ? query.getResultList() : query.getSingleResult();
		} catch (NoResultException e) {
			return isList ? Collections.emptyList() : null;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}

	protected void handleError() {
		if (entityManager != null) {
			entityManager.getTransaction().rollback();
		}
	}

	protected void closeSession() {
		if (entityManager != null) {
			entityManager.close();
		}
	}
}
