package com.mkm.publicservices.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.mkm.publicservices.dto.LoginDTO;
import com.mkm.publicservices.manager.LoginManager;
import com.mkm.publicservices.manager.impl.LoginManagerImpl;

@Path("login")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class LoginService {

	private LoginManager manager = new LoginManagerImpl();
	@Context private UriInfo uriInfo;
	
	@POST
	public Response login(LoginDTO dto) {
		return manager.login(dto, uriInfo);
	}
}
