package com.mkm.publicservices.service;
import javax.ws.rs.Consumes;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.mkm.publicservices.annotation.JWTTokenNeeded;
import com.mkm.publicservices.constant.Singleton;
import com.mkm.publicservices.manager.UsuarioManager;
import com.mkm.publicservices.manager.impl.UsuarioManagerImpl;
import com.mkm.publicservices.model.Usuario;

@JWTTokenNeeded
@Path("usuarios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsuarioService {
	
	private UsuarioManager manager = new UsuarioManagerImpl();
	private Gson gson = Singleton.GSON;

	@Context
	private UriInfo uriInfo;

	@GET
	public String findAll() {
		MultivaluedMap<String,String> queryParams = uriInfo.getQueryParameters();
		return gson.toJson(manager.findAll(queryParams));
	}

	@GET
	@Path("{id}")
	public String find(@PathParam("id") int id) {
		return gson.toJson(manager.find(id));
	}
	
	@POST
	public Response save(String usuario) {
		Usuario u = gson.fromJson(usuario, Usuario.class);
		return Response.ok(gson.toJson(manager.save(u))).build();
	}

	@PUT
	@Path("{id}")
	public Response update(@PathParam("id") int id, String usuario) {
		Usuario u = gson.fromJson(usuario, Usuario.class);
		return Response.ok(gson.toJson(manager.update(id, u))).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		manager.delete(id);
		return Response.noContent().build();
	}
}
