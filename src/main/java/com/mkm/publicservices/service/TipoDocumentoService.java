package com.mkm.publicservices.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.mkm.publicservices.constant.Singleton;
import com.mkm.publicservices.manager.TipoDocumentoManager;
import com.mkm.publicservices.manager.impl.TipoDocumentoManagerImpl;

@Path("tipo-documento")
@Produces(MediaType.APPLICATION_JSON)
public class TipoDocumentoService {

	private TipoDocumentoManager manager = new TipoDocumentoManagerImpl();

	@Context
	private UriInfo uriInfo;
	private Gson gson = Singleton.GSON;

	@GET
	public String findAll() {
		MultivaluedMap<String,String> queryParams = uriInfo.getQueryParameters();
		return gson.toJson(manager.findAll(queryParams));
	}

	@GET
	@Path("{id}")
	public String findAll(@PathParam("id") int id) {
		return gson.toJson(manager.find(id));
	}
}