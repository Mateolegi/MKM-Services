package com.mkm.publicservices.service;
import javax.ws.rs.Consumes;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.mkm.publicservices.constant.Singleton;
import com.mkm.publicservices.manager.TipoPerfilManager;
import com.mkm.publicservices.manager.impl.TipoPerfilManagerImpl;
import com.mkm.publicservices.model.TipoPerfil;

@Path("tipo-perfil")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TipoPerfilService {
	private TipoPerfilManager manager = new TipoPerfilManagerImpl();
	
	@Context
	private UriInfo uriInfo;
	private Gson gson = Singleton.GSON;

	@GET
	public String findAll() {
		MultivaluedMap<String,String> queryParams = uriInfo.getQueryParameters();
		return gson.toJson(manager.findAll(queryParams));
	}

	@GET
	@Path("{id}")
	public String find(@PathParam("id") int id) {
		return gson.toJson(manager.find(id));
	}
	
	@POST
	public Response save(TipoPerfil tipoPerfil) {
		manager.save(tipoPerfil);
		return Response.ok().build();
	}

	@PUT
	@Path("{id}")
	public Response update(@PathParam("id") int id, TipoPerfil tipoPerfil) {
		return Response.ok(manager.update(id, tipoPerfil)).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		manager.delete(id);
		return Response.noContent().build();
	}

}
