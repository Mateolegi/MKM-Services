package com.mkm.publicservices.manager;

import java.util.Date;
import java.util.List;

import com.mkm.publicservices.model.AsignacionSubsidio;
import com.mkm.publicservices.model.Hogar;
import com.mkm.publicservices.model.LiquidacionCicloVivienda;
import com.mkm.publicservices.model.Usuario;

import io.github.mateolegi.manager.CrudManager;

public interface AsignacionSubsidioManager extends CrudManager<AsignacionSubsidio> {

	List<LiquidacionCicloVivienda> generarLiquidacion(Date fechaDesde, Date fechaHasta, String jwtToken);

	List<LiquidacionCicloVivienda> generarSubsidioHogar(Hogar hogar, Date fechaDesde, Date fechaHasta, Usuario usuario);
}
