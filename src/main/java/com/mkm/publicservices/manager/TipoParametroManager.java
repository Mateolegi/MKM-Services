package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoParametro;

import io.github.mateolegi.manager.CrudManager;

public interface TipoParametroManager extends CrudManager<TipoParametro> {

}
