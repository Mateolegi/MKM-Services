package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.LiquidacionValorConsumo;

import io.github.mateolegi.manager.CrudManager;

public interface LiquidacionValorConsumoManager extends CrudManager<LiquidacionValorConsumo> {

}
