package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoConsumo;

import io.github.mateolegi.manager.CrudManager;

public interface TipoConsumoManager extends CrudManager<TipoConsumo> {

}
