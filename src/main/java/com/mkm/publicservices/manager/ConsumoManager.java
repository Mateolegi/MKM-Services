package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.Consumo;

import io.github.mateolegi.manager.CrudManager;

public interface ConsumoManager extends CrudManager<Consumo> {

}
