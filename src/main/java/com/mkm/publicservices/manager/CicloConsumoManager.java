package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.CicloConsumo;

import io.github.mateolegi.manager.CrudManager;

public interface CicloConsumoManager extends CrudManager<CicloConsumo> {

}
