package com.mkm.publicservices.manager;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.mkm.publicservices.dto.LoginDTO;
import com.mkm.publicservices.model.Usuario;

import io.jsonwebtoken.Claims;

public interface LoginManager {

	Response login(LoginDTO dto, UriInfo uriInfo);

	Usuario validarCredenciales(LoginDTO dto);

	String generateToken(int id, String user, String uri);

	Claims parseJWT(String jwt);
}
