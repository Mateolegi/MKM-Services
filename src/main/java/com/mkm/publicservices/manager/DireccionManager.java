package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.Direccion;

import io.github.mateolegi.manager.CrudManager;

public interface DireccionManager extends CrudManager<Direccion> {

}
