package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.Usuario;

import io.github.mateolegi.manager.CrudManager;

public interface UsuarioManager extends CrudManager<Usuario> {

	Usuario getUsuarioJWT(String jwt);
}
