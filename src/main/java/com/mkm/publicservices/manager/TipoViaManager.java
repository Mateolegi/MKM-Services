package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoVia;

import io.github.mateolegi.manager.CrudManager;

public interface TipoViaManager extends CrudManager<TipoVia> {

}
