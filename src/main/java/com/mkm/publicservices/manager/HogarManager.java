package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.Hogar;

import io.github.mateolegi.manager.CrudManager;

public interface HogarManager extends CrudManager<Hogar> {

	Hogar save(Hogar hogar, String authorizationHeader);
}
