package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.PromedioConsumoCiclo;

import io.github.mateolegi.manager.CrudManager;

public interface PromedioConsumoCicloManager extends CrudManager<PromedioConsumoCiclo> {

}
