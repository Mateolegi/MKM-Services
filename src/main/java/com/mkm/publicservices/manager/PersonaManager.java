package com.mkm.publicservices.manager;

import java.util.List;

import com.mkm.publicservices.model.Persona;

import io.github.mateolegi.manager.CrudManager;

public interface PersonaManager extends CrudManager<Persona> {

	/**
	 * Obtiene todas las personas que tienen uno o más hogares
	 * @return
	 */
	List<Persona> getPropietarios();
}
