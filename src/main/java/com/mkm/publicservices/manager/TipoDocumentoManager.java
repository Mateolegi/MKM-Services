package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoDocumento;

import io.github.mateolegi.manager.CrudManager;

public interface TipoDocumentoManager extends CrudManager<TipoDocumento> {

}
