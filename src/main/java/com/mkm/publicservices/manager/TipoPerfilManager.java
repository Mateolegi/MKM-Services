package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoPerfil;

import io.github.mateolegi.manager.CrudManager;

public interface TipoPerfilManager extends CrudManager<TipoPerfil> {

}
