package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoConsumoDao;
import com.mkm.publicservices.dao.impl.TipoConsumoDaoImpl;
import com.mkm.publicservices.manager.TipoConsumoManager;
import com.mkm.publicservices.model.TipoConsumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoConsumoManagerImpl extends CrudManagerImpl<TipoConsumo> implements TipoConsumoManager {
	private static final TipoConsumoDao TCD = new TipoConsumoDaoImpl();
	
	public TipoConsumoManagerImpl() {
		super(TCD);
	}

}
