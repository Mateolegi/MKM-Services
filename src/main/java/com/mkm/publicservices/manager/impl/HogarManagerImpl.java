package com.mkm.publicservices.manager.impl;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.HogarDao;
import com.mkm.publicservices.dao.impl.HogarDaoImpl;
import com.mkm.publicservices.manager.DireccionManager;
import com.mkm.publicservices.manager.HogarManager;
import com.mkm.publicservices.manager.LoginManager;
import com.mkm.publicservices.manager.PersonaManager;
import com.mkm.publicservices.manager.UsuarioManager;
import com.mkm.publicservices.model.Hogar;
import com.mkm.publicservices.model.Usuario;

import io.github.mateolegi.manager.impl.CrudManagerImpl;
import io.jsonwebtoken.Claims;

public class HogarManagerImpl extends CrudManagerImpl<Hogar> implements HogarManager {
	private static final HogarDao HOGAR_DAO = new HogarDaoImpl();
	private static final DireccionManager DIRECCION_MANAGER = new DireccionManagerImpl();
	private static final PersonaManager PERSONA_MANAGER = new PersonaManagerImpl(); 
	private static final UsuarioManager USUARIO_DAO = new UsuarioManagerImpl();
	private static final LoginManager LOGIN_MANAGER = new LoginManagerImpl();
	private static final Logger LOGGER = LogManager.getLogger(HogarManagerImpl.class);

	public HogarManagerImpl() {
		super(HOGAR_DAO);
	}

	@Override
	public Hogar save(Hogar hogar, String authorizationHeader) {
		hogar.setDireccion(DIRECCION_MANAGER.save(hogar.getDireccion()));
		hogar.setUsuario(getUsuario(authorizationHeader));
		return super.save(hogar);
	}

	@Override
	public Hogar update(int id, Hogar hogar) {
		if (id == hogar.getIdHogar()) {
			DIRECCION_MANAGER.update(hogar.getDireccion().getIdDireccion(), hogar.getDireccion());
			PERSONA_MANAGER.update(hogar.getResponsable().getIdPersona(), hogar.getResponsable());
			return HOGAR_DAO.update(hogar);
		}
		throw new BadRequestException("El identificador no coincide");
	}

	private Usuario getUsuario(String authorizationHeader) {
		LOGGER.debug(String.format("JWT: %s", authorizationHeader));
		try {
			Claims claims = LOGIN_MANAGER.parseJWT(authorizationHeader.split("Bearer ")[1]);
			return USUARIO_DAO.find(Integer.parseInt(claims.getId()));
		} catch (Exception e) {
			LOGGER.error("Ocurrió un error obteniendo el usuario", e);
			throw new ServerErrorException(500);
		}
	}

	@Override
	public void delete(int id) {
		Hogar entity = find(id);
		if (entity != null) {
			DIRECCION_MANAGER.delete(entity.getDireccion().getIdDireccion());
			super.delete(id);
		}
		throw new BadRequestException("No existe una entidad con ese identificador");
	}
}
