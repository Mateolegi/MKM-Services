package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.PromedioCicloConsumoDao;
import com.mkm.publicservices.dao.impl.PromedioCicloConsumoDaoImpl;
import com.mkm.publicservices.manager.PromedioConsumoCicloManager;
import com.mkm.publicservices.model.PromedioConsumoCiclo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class PromedioConsumoCicloManagerImpl extends CrudManagerImpl<PromedioConsumoCiclo> implements PromedioConsumoCicloManager {
	private static final PromedioCicloConsumoDao PCCD= new PromedioCicloConsumoDaoImpl();
	public PromedioConsumoCicloManagerImpl() {
		super(PCCD);
	}
}
