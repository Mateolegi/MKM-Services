package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.DireccionDao;
import com.mkm.publicservices.dao.impl.DireccionDaoImpl;
import com.mkm.publicservices.manager.DireccionManager;
import com.mkm.publicservices.model.Direccion;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class DireccionManagerImpl extends CrudManagerImpl<Direccion> implements DireccionManager{

	private static final DireccionDao direccionDao= new DireccionDaoImpl();
	public DireccionManagerImpl() {
		super(direccionDao);
	}
}
