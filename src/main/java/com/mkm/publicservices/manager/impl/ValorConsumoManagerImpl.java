package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.ValorConsumoDao;
import com.mkm.publicservices.dao.impl.ValorConsumoDaoImpl;
import com.mkm.publicservices.manager.ValorConsumoManager;
import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class ValorConsumoManagerImpl extends CrudManagerImpl<ValorConsumo> implements ValorConsumoManager {
	private static final ValorConsumoDao VCD = new ValorConsumoDaoImpl();
	public ValorConsumoManagerImpl() {
		super(VCD);
	}

}
