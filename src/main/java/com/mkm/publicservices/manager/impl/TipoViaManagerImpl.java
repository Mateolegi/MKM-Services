package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoViaDao;
import com.mkm.publicservices.dao.impl.TipoViaDaoImpl;
import com.mkm.publicservices.manager.TipoViaManager;
import com.mkm.publicservices.model.TipoVia;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoViaManagerImpl extends CrudManagerImpl<TipoVia> implements TipoViaManager {

	private static final TipoViaDao tipoViaDao = new TipoViaDaoImpl();

	public TipoViaManagerImpl() {
		super(tipoViaDao);
	}
}
