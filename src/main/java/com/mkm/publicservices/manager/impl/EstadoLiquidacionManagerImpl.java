package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.EstadoLiquidacionDao;
import com.mkm.publicservices.dao.impl.EstadoLiquidacionDaoImpl;
import com.mkm.publicservices.manager.EstadoLiquidacionManager;
import com.mkm.publicservices.model.EstadoLiquidacion;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class EstadoLiquidacionManagerImpl extends CrudManagerImpl<EstadoLiquidacion> implements EstadoLiquidacionManager {
	private static final EstadoLiquidacionDao estadoliquidacion = new EstadoLiquidacionDaoImpl();
	public EstadoLiquidacionManagerImpl() {
		super(estadoliquidacion);
	}
}
