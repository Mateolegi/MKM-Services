package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.CicloConsumoDao;
import com.mkm.publicservices.dao.impl.CicloConsumoDaoImpl;
import com.mkm.publicservices.manager.CicloConsumoManager;
import com.mkm.publicservices.model.CicloConsumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class CicloConsumoManagerImpl extends CrudManagerImpl<CicloConsumo> implements CicloConsumoManager {
	
	private static final CicloConsumoDao cicloDao = new CicloConsumoDaoImpl();
	
	public CicloConsumoManagerImpl() {
		super(cicloDao);
		
	}

}
