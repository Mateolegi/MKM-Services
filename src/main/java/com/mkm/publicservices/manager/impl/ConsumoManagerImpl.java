package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.ConsumoDao;
import com.mkm.publicservices.manager.ConsumoManager;
import com.mkm.publicservices.model.Consumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class ConsumoManagerImpl extends CrudManagerImpl<Consumo> implements ConsumoManager {

	private static ConsumoDao consumoDao = new ConsumoDaoImpl();

	public ConsumoManagerImpl() {
		super(consumoDao);
	}
}
