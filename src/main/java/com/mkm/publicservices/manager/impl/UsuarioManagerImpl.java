package com.mkm.publicservices.manager.impl;

import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.PersistenceException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

import com.mkm.publicservices.dao.UsuarioDao;
import com.mkm.publicservices.dao.impl.UsuarioDaoImpl;
import com.mkm.publicservices.manager.LoginManager;
import com.mkm.publicservices.manager.PersonaManager;
import com.mkm.publicservices.manager.UsuarioManager;
import com.mkm.publicservices.model.Usuario;
import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class UsuarioManagerImpl extends CrudManagerImpl<Usuario> implements UsuarioManager {
	private static final UsuarioDao USUARIO_DAO = new UsuarioDaoImpl();
	private static final PersonaManager PERSONA_MANAGER = new PersonaManagerImpl();
	private static final LoginManager LOGIN_MANAGER = new LoginManagerImpl();

	public UsuarioManagerImpl() {
		super(USUARIO_DAO);
	}

	@Override
	public Usuario save(Usuario entity) {
		try {
			if (entity.getPersona().getIdPersona() == 0) {
				entity.setPersona(PERSONA_MANAGER.save(entity.getPersona()));
			} else {
				entity.setPersona(PERSONA_MANAGER.find(entity.getPersona().getIdPersona()));
			}
			entity.setContrasena(entity.getPersona().getNumeroDocumento());
			entity.setFechaCreacion(Timestamp.from(Instant.now()));
			return super.save(entity);
		} catch (PersistenceException e) {
			throw new ClientErrorException(Response.status(401)
					.entity("Ya existe un usuario para esa persona.").build());
		} catch (Exception e) {
			throw new ServerErrorException(500);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.mkm.publicservices.manager.UsuarioManager#getUsuarioJWT(java.lang.String)
	 */
	@Override
	public Usuario getUsuarioJWT(String jwt) {
		return USUARIO_DAO.find(Integer.parseInt(LOGIN_MANAGER.parseJWT(jwt).getId()));
	}
}
