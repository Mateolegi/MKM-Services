package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.ConsumoDao;
import com.mkm.publicservices.model.Consumo;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class ConsumoDaoImpl extends CrudDaoImpl<Consumo> implements ConsumoDao {

	public ConsumoDaoImpl() {
		super(Consumo.class);
	}
}
