package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.LiquidacionCicloViviendaDao;
import com.mkm.publicservices.dao.impl.LiquidacionCicloViviendaDaoImpl;
import com.mkm.publicservices.manager.LiquidacionCicloViviendaManager;
import com.mkm.publicservices.model.LiquidacionCicloVivienda;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class LiquidacionCicloViviendaManagerImpl extends CrudManagerImpl<LiquidacionCicloVivienda> implements LiquidacionCicloViviendaManager {
	private static final LiquidacionCicloViviendaDao LCV = new LiquidacionCicloViviendaDaoImpl();
	public LiquidacionCicloViviendaManagerImpl() {
		super(LCV);
	}
}
