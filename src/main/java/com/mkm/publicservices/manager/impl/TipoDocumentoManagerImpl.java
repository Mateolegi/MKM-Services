package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoDocumentoDao;
import com.mkm.publicservices.dao.impl.TipoDocumentoDaoImpl;
import com.mkm.publicservices.manager.TipoDocumentoManager;
import com.mkm.publicservices.model.TipoDocumento;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoDocumentoManagerImpl extends CrudManagerImpl<TipoDocumento> implements TipoDocumentoManager {

	private static TipoDocumentoDao dao = new TipoDocumentoDaoImpl();

	public TipoDocumentoManagerImpl() {
		super(dao);
	}
}
