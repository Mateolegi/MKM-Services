package com.mkm.publicservices.manager.impl;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import com.mkm.publicservices.dao.PersonaDao;
import com.mkm.publicservices.dao.impl.PersonaDaoImpl;
import com.mkm.publicservices.manager.PersonaManager;
import com.mkm.publicservices.model.Persona;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class PersonaManagerImpl extends CrudManagerImpl<Persona> implements PersonaManager {
	private static final PersonaDao personaDao = new PersonaDaoImpl();
	public PersonaManagerImpl() {
		super(personaDao);
	}

	@Override
	public Persona save(Persona persona) {
		persona.setFechaRegistro(Timestamp.from(Instant.now()));
		return super.save(persona);
	}

	@Override
	public List<Persona> getPropietarios() {
		return personaDao.getPropietarios();
	}
}
