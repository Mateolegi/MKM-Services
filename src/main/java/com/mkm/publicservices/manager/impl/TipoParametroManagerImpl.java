package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoParametroDao;
import com.mkm.publicservices.dao.impl.TipoParametroDaoImpl;
import com.mkm.publicservices.manager.TipoParametroManager;
import com.mkm.publicservices.model.TipoParametro;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoParametroManagerImpl extends CrudManagerImpl<TipoParametro> implements TipoParametroManager{
	private static final TipoParametroDao tipoParametro = new TipoParametroDaoImpl();
	
	public TipoParametroManagerImpl () {
		super(tipoParametro);
	}
	

}
