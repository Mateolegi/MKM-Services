package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoPerfilDao;
import com.mkm.publicservices.dao.impl.TipoPerfilDaoImpl;
import com.mkm.publicservices.manager.TipoPerfilManager;
import com.mkm.publicservices.model.TipoPerfil;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoPerfilManagerImpl extends CrudManagerImpl<TipoPerfil> implements TipoPerfilManager {

	private static final TipoPerfilDao TIPO_PERFIL_DAO = new TipoPerfilDaoImpl();
	
	public TipoPerfilManagerImpl() {
		super(TIPO_PERFIL_DAO);
		
	}

}
