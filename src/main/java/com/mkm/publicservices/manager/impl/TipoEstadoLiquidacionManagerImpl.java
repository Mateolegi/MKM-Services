package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.TipoEstadoLiquidacionDao;
import com.mkm.publicservices.dao.impl.TipoEstadoLiquidacionDaoImpl;
import com.mkm.publicservices.manager.TipoEstadoLiquidacionManager;
import com.mkm.publicservices.model.TipoEstadoLiquidacion;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class TipoEstadoLiquidacionManagerImpl extends CrudManagerImpl<TipoEstadoLiquidacion> implements TipoEstadoLiquidacionManager {
private static final TipoEstadoLiquidacionDao TELD= new TipoEstadoLiquidacionDaoImpl();
public TipoEstadoLiquidacionManagerImpl() {
	super(TELD);
}
}
