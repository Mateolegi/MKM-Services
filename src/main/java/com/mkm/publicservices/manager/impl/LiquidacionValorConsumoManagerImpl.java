package com.mkm.publicservices.manager.impl;

import com.mkm.publicservices.dao.LiquidacionValorConsumoDao;
import com.mkm.publicservices.dao.impl.LiquidacionValorConsumoDaoImpl;
import com.mkm.publicservices.manager.LiquidacionValorConsumoManager;
import com.mkm.publicservices.model.LiquidacionValorConsumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class LiquidacionValorConsumoManagerImpl extends CrudManagerImpl<LiquidacionValorConsumo>implements LiquidacionValorConsumoManager{

	private static final LiquidacionValorConsumoDao LVCD= new LiquidacionValorConsumoDaoImpl();
public LiquidacionValorConsumoManagerImpl() {
	super(LVCD);
}
}
