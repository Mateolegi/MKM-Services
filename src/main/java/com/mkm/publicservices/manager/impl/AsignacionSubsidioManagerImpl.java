package com.mkm.publicservices.manager.impl;

import static com.mkm.publicservices.constant.TipoEstadoLiquidacion.GENERADO;
import static com.mkm.publicservices.constant.TipoParametro.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.AsignacionSubsidioDao;
import com.mkm.publicservices.dao.CicloConsumoDao;
import com.mkm.publicservices.dao.EstadoLiquidacionDao;
import com.mkm.publicservices.dao.HogarDao;
import com.mkm.publicservices.dao.LiquidacionCicloViviendaDao;
import com.mkm.publicservices.dao.LiquidacionValorConsumoDao;
import com.mkm.publicservices.dao.TipoEstadoLiquidacionDao;
import com.mkm.publicservices.dao.TipoParametroDao;
import com.mkm.publicservices.dao.ValorConsumoDao;
import com.mkm.publicservices.dao.impl.AsignacionSubsidioDaoImpl;
import com.mkm.publicservices.dao.impl.CicloConsumoDaoImpl;
import com.mkm.publicservices.dao.impl.EstadoLiquidacionDaoImpl;
import com.mkm.publicservices.dao.impl.HogarDaoImpl;
import com.mkm.publicservices.dao.impl.LiquidacionCicloViviendaDaoImpl;
import com.mkm.publicservices.dao.impl.LiquidacionValorConsumoDaoImpl;
import com.mkm.publicservices.dao.impl.TipoEstadoLiquidacionDaoImpl;
import com.mkm.publicservices.dao.impl.TipoParametroDaoImpl;
import com.mkm.publicservices.dao.impl.ValorConsumoDaoImpl;
import com.mkm.publicservices.manager.AsignacionSubsidioManager;
import com.mkm.publicservices.manager.UsuarioManager;
import com.mkm.publicservices.model.AsignacionSubsidio;
import com.mkm.publicservices.model.CicloConsumo;
import com.mkm.publicservices.model.Consumo;
import com.mkm.publicservices.model.EstadoLiquidacion;
import com.mkm.publicservices.model.Hogar;
import com.mkm.publicservices.model.LiquidacionCicloVivienda;
import com.mkm.publicservices.model.LiquidacionValorConsumo;
import com.mkm.publicservices.model.TipoParametro;
import com.mkm.publicservices.model.Usuario;
import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.manager.impl.CrudManagerImpl;

public class AsignacionSubsidioManagerImpl extends CrudManagerImpl<AsignacionSubsidio> 
		implements AsignacionSubsidioManager {

	private static final Logger LOGGER = LogManager.getLogger(AsignacionSubsidioManagerImpl.class);

	private static final AsignacionSubsidioDao ASIGNACION_SUBSIDIO_DAO = new AsignacionSubsidioDaoImpl();
	private static final CicloConsumoDao CICLO_CONSUMO_DAO = new CicloConsumoDaoImpl();
	private static final EstadoLiquidacionDao ESTADO_LIQUIDACION_DAO = new EstadoLiquidacionDaoImpl();
	private static final LiquidacionCicloViviendaDao LIQUIDACION_CICLO_VIVIENDA_DAO = new LiquidacionCicloViviendaDaoImpl();
	private static final LiquidacionValorConsumoDao LIQUIDACION_VALOR_CONSUMO_DAO = new LiquidacionValorConsumoDaoImpl();
	private static final HogarDao HOGAR_DAO = new HogarDaoImpl();
	private static final TipoEstadoLiquidacionDao TIPO_ESTADO_LIQUIDACION_DAO = new TipoEstadoLiquidacionDaoImpl();
	private static final TipoParametroDao TIPO_PARAMETRO_DAO = new TipoParametroDaoImpl();
	private static final ValorConsumoDao VALOR_CONSUMO_DAO = new ValorConsumoDaoImpl();
	
	private static final UsuarioManager USUARIO_MANAGER = new UsuarioManagerImpl();

	public AsignacionSubsidioManagerImpl() {
		super(ASIGNACION_SUBSIDIO_DAO);
	}

	@Override
	public List<LiquidacionCicloVivienda> generarLiquidacion(Date fechaDesde, Date fechaHasta, String authorizationHeader) {
		System.out.println("Obtiene el usuario");
		String token = authorizationHeader.substring("Bearer".length()).trim();
		Usuario usuario = USUARIO_MANAGER.getUsuarioJWT(token);
		System.out.println("Obtiene hogares");
		List<Hogar> hogares = HOGAR_DAO.findAll();
		List<LiquidacionCicloVivienda> respuesta = new ArrayList<>();
		for (Hogar hogar : hogares) {
			System.out.println("Procedimiento para hogar " + hogar.getIdHogar());
			respuesta.addAll(generarSubsidioHogar(hogar, fechaDesde, fechaHasta, usuario));
		}
//		registarPromedioConsumoCiclos(respuesta);
		return LIQUIDACION_CICLO_VIVIENDA_DAO.findAll();
	}

	@Override
	public List<LiquidacionCicloVivienda> generarSubsidioHogar(Hogar hogar, Date fechaDesde, Date fechaHasta, Usuario usuario) {
		List<LiquidacionCicloVivienda> listLiquidacionCicloVivienda = new ArrayList<>();
		// Se obtienen los ciclos que se incluyen entre las fechas para el hogar
		System.out.println("Obtiene los ciclos para el rango");
		List<CicloConsumo> listCicloConsumo = CICLO_CONSUMO_DAO
				.obtenerCiclosConsumoRango(hogar, fechaDesde, fechaHasta);
		for (CicloConsumo cicloConsumo : listCicloConsumo) {
			System.out.println("Registra liquidación ciclo vivienda");
			LiquidacionCicloVivienda liquidacionCicloVivienda = registrarLiquidacionCicloVivienda(hogar, 
					cicloConsumo);
			listLiquidacionCicloVivienda.add(liquidacionCicloVivienda);
			registrarEstadoLiquidacion(liquidacionCicloVivienda, usuario);
			boolean aplicaSubsidio = false;
			System.out.println("Obtiene consumos");
			for (Consumo consumo : cicloConsumo.getConsumos()) {
				// Obtenemos el valor de la unidad del consumo para el año de la fecha inicial
				System.out.println("Obtiene el valor para el consumo en el año");
				ValorConsumo valorConsumo = VALOR_CONSUMO_DAO
						.obtenerValorConsumoTipoAnno(consumo.getTipoConsumo(), fechaDesde);
				if (valorConsumo == null) {
					throw new BadRequestException(Response.status(400)
							.entity(String.format("No existe un valor parametrizado para el consumo %s en el año %s", 
									consumo.getTipoConsumo().getTipoConsumo(), fechaDesde.getYear())).build());
				}
				// Obtenemos el valor parametrizado para el subsidio del consumo
				System.out.println("Obtiene el valor parametrizado para el subsidio");
				BigDecimal valorParametro = obtenerValorSubsidioParametro(consumo
						.getTipoConsumo().getIdTipoConsumo());
				if (valorParametro == null) {
					throw new BadRequestException(Response.status(400)
							.entity(String.format("No existe un valor parametrizado para el subsidio del consumo %s ", 
									consumo.getTipoConsumo().getTipoConsumo())).build());
				}
				// Multiplicamos el valor de la unidad por el total del consumo del ciclo
				System.out.println("Calculando valor de consumo");
				BigDecimal valor = valorConsumo.getValorConsumo().multiply(BigDecimal.valueOf(consumo.getConsumo()));
				System.out.println("Valor consumo: " + valor);
				aplicaSubsidio = aplicarSubsidio(valorParametro, valor, consumo);
				System.out.println("Aplica subsidio: " + aplicaSubsidio);
				System.out.println("Registra Liquidación valor consumo");
				registrarLiquidacionValorConsumo(liquidacionCicloVivienda, consumo, valorParametro, valor, aplicaSubsidio);
			}
			if (aplicaSubsidio) {
				registrarAsignacionSubsidio(hogar, cicloConsumo);
			}
		}
		return listLiquidacionCicloVivienda;
	}

	private EstadoLiquidacion registrarEstadoLiquidacion(LiquidacionCicloVivienda liquidacionCicloVivienda, Usuario usuario) {
		EstadoLiquidacion estadoLiquidacion = new EstadoLiquidacion();
		estadoLiquidacion.setLiquidacionCicloVivienda(liquidacionCicloVivienda);
		estadoLiquidacion.setTipoEstadoLiquidacion(TIPO_ESTADO_LIQUIDACION_DAO.find(GENERADO));
		estadoLiquidacion.setFechaRegistro(Timestamp.valueOf(LocalDateTime.now()));
		estadoLiquidacion.setUsuario(usuario);
		return ESTADO_LIQUIDACION_DAO.save(estadoLiquidacion);
	}

//	private void registarPromedioConsumoCiclos(List<List<AsignacionSubsidio>> listDobleAsignacionSubsidio) {
//		for (List<AsignacionSubsidio> listAsignacionSubsidio : listDobleAsignacionSubsidio) {
//			for (AsignacionSubsidio asignacionSubsidio : listAsignacionSubsidio) {
//				createPromedioConsumoCiclo(asignacionSubsidio);
//			}
//		}
//	}

//	private void createPromedioConsumoCiclo(AsignacionSubsidio asignacionSubsidio) {
//		 TODO: Implementar registros en la tabla PROMEDIO_CONSUMO_CICLO
//	}

	private LiquidacionCicloVivienda registrarLiquidacionCicloVivienda(Hogar hogar, 
			CicloConsumo cicloConsumo) {
		LiquidacionCicloVivienda liquidacionCicloVivienda = new LiquidacionCicloVivienda();
		liquidacionCicloVivienda.setHogar(hogar);
		liquidacionCicloVivienda.setCicloConsumo(cicloConsumo);
		return LIQUIDACION_CICLO_VIVIENDA_DAO.save(liquidacionCicloVivienda);
	}

	private LiquidacionValorConsumo registrarLiquidacionValorConsumo(LiquidacionCicloVivienda liquidacionCicloVivienda, 
			Consumo consumo, BigDecimal valorParametro, BigDecimal valor, boolean aplicaSubsidio) {
		LiquidacionValorConsumo liquidacionValorConsumo = new LiquidacionValorConsumo();
		liquidacionValorConsumo.setLiquidacionCicloVivienda(liquidacionCicloVivienda);
		liquidacionValorConsumo.setConsumo(consumo);
		liquidacionValorConsumo.setAplicaSubsidio(aplicaSubsidio ? (byte) 1 : (byte) 0);
		// Aplica para subsidio
		if (aplicaSubsidio) {
			// subsidio = valor del consumo - (valor del parámetro * 0.5)
			BigDecimal valorSubsidiado = valor.subtract(valorParametro.multiply(BigDecimal.valueOf(0.5d)));
			liquidacionValorConsumo.setValorConsumo(valorSubsidiado
					.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : valorSubsidiado);
		} else {
			liquidacionValorConsumo.setValorConsumo(valor);
		}
		System.out.println("Valor consumo para liquidación: " + liquidacionValorConsumo.getValorConsumo());
		return LIQUIDACION_VALOR_CONSUMO_DAO.save(liquidacionValorConsumo);
	}

	private AsignacionSubsidio registrarAsignacionSubsidio(Hogar hogar, CicloConsumo cicloConsumo) {
		AsignacionSubsidio asignacionSubsidio = new AsignacionSubsidio();
		asignacionSubsidio.setHogar(hogar);
		asignacionSubsidio.setCicloConsumo(cicloConsumo);
		return ASIGNACION_SUBSIDIO_DAO.save(asignacionSubsidio);
	}

	private boolean aplicarSubsidio(BigDecimal valorParametro, BigDecimal valor, Consumo consumo) {
		return valorParametro != null && valorParametro.compareTo(valor) <= 0;
	}

	private BigDecimal obtenerValorSubsidioParametro(int idTipoConsumo) {
		TipoParametro tipoParametro;
		switch (idTipoConsumo) {
		case ENERGIA:
			tipoParametro = TIPO_PARAMETRO_DAO.findByName("VALOR-SUBSIDIO-ENERGIA");
			return new BigDecimal(tipoParametro.getValorParametro());
		case ACUEDUCTO:
			tipoParametro = TIPO_PARAMETRO_DAO.findByName("VALOR-SUBSIDIO-ACUEDUCTO");
			return new BigDecimal(tipoParametro.getValorParametro());
		case ALCANTARILLADO:
			tipoParametro = TIPO_PARAMETRO_DAO.findByName("VALOR-SUBSIDIO-ALCANTARILLADO");
			return new BigDecimal(tipoParametro.getValorParametro());
		case GAS:
			tipoParametro = TIPO_PARAMETRO_DAO.findByName("VALOR-SUBSIDIO-GAS");
			return new BigDecimal(tipoParametro.getValorParametro());
		default:
			return null;
		}
	}
}
