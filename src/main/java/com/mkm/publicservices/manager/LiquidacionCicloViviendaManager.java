package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.LiquidacionCicloVivienda;

import io.github.mateolegi.manager.CrudManager;

public interface LiquidacionCicloViviendaManager extends CrudManager<LiquidacionCicloVivienda> {

}
