package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.manager.CrudManager;

public interface ValorConsumoManager extends CrudManager<ValorConsumo> {

}
