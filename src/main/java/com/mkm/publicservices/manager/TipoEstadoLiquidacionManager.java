package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.TipoEstadoLiquidacion;

import io.github.mateolegi.manager.CrudManager;

public interface TipoEstadoLiquidacionManager extends CrudManager<TipoEstadoLiquidacion> {

}
