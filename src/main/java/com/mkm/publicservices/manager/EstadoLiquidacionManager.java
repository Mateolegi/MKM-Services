package com.mkm.publicservices.manager;

import com.mkm.publicservices.model.EstadoLiquidacion;

import io.github.mateolegi.manager.CrudManager;

public interface EstadoLiquidacionManager extends CrudManager<EstadoLiquidacion> {

}
