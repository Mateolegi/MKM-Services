package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoVia;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoViaDao extends CrudDao<TipoVia> {

}
