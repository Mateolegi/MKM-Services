package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.PromedioConsumoCiclo;

import io.github.mateolegi.database.dao.CrudDao;

public interface PromedioCicloConsumoDao extends CrudDao<PromedioConsumoCiclo> {

}
