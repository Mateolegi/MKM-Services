package com.mkm.publicservices.dao;

import java.util.Date;
import java.util.List;

import com.mkm.publicservices.model.CicloConsumo;
import com.mkm.publicservices.model.Hogar;

import io.github.mateolegi.database.dao.CrudDao;

public interface CicloConsumoDao extends CrudDao<CicloConsumo> {

	List<CicloConsumo> obtenerCiclosConsumoRango(Hogar hogar, Date fechaDesde, Date fechaHasta);
}
