package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.Consumo;

import io.github.mateolegi.database.dao.CrudDao;

public interface ConsumoDao extends CrudDao<Consumo> {

}
