package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.Usuario;

import io.github.mateolegi.database.dao.CrudDao;

public interface UsuarioDao extends CrudDao<Usuario> {

	Usuario findByUsername(String username);

	Usuario findByEmail(String email);
}
