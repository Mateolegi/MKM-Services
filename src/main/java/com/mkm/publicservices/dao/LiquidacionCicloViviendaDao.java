package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.LiquidacionCicloVivienda;

import io.github.mateolegi.database.dao.CrudDao;

public interface LiquidacionCicloViviendaDao extends CrudDao<LiquidacionCicloVivienda> {

}
