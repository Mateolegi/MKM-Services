package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoParametro;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoParametroDao extends CrudDao<TipoParametro> {

	TipoParametro findByName(String tipoParametro);
}
