package com.mkm.publicservices.dao;

import java.util.Date;

import com.mkm.publicservices.model.TipoConsumo;
import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.database.dao.CrudDao;

public interface ValorConsumoDao extends CrudDao<ValorConsumo> {

	ValorConsumo obtenerValorConsumoTipoAnno(TipoConsumo tipoConsumo, Date anno);
}
