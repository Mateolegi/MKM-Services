package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoDocumento;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoDocumentoDao extends CrudDao<TipoDocumento> {

}
