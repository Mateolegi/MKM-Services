package com.mkm.publicservices.dao;

import java.util.List;

import com.mkm.publicservices.model.Persona;

import io.github.mateolegi.database.dao.CrudDao;

public interface PersonaDao extends CrudDao<Persona> {

	/**
	 * Obtiene todas las personas que tienen uno o más hogares
	 * @return
	 */
	List<Persona> getPropietarios();
}
