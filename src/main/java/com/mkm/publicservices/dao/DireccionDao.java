package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.Direccion;

import io.github.mateolegi.database.dao.CrudDao;

public interface DireccionDao extends CrudDao<Direccion> {

}
