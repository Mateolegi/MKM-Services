package com.mkm.publicservices.dao.impl;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.TipoParametroDao;
import com.mkm.publicservices.model.TipoParametro;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;
import io.github.mateolegi.util.HibernateUtil;
import io.github.mateolegi.util.TransactionException;

public class TipoParametroDaoImpl extends CrudDaoImpl<TipoParametro> implements TipoParametroDao {

	private static final Logger LOGGER = LogManager.getLogger(TipoParametroDaoImpl.class);

	public TipoParametroDaoImpl() {
		super(TipoParametro.class);
	}

	@Override
	public TipoParametro findByName(String tipoParametro) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			TipoParametro entity = entityManager.createNamedQuery("TipoParametro.findByName", TipoParametro.class)
				.setParameter("tipoParametro", tipoParametro)
				.getSingleResult();
			return entity;
		} catch (NoResultException e) {
			return null;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}
}
