package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.HogarDao;
import com.mkm.publicservices.model.Hogar;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class HogarDaoImpl extends CrudDaoImpl<Hogar> implements HogarDao{
	public HogarDaoImpl() {
		super(Hogar.class);
	}

}
