package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.TipoEstadoLiquidacionDao;
import com.mkm.publicservices.model.TipoEstadoLiquidacion;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class TipoEstadoLiquidacionDaoImpl extends CrudDaoImpl<TipoEstadoLiquidacion> implements TipoEstadoLiquidacionDao{

	public TipoEstadoLiquidacionDaoImpl() {
		super(TipoEstadoLiquidacion.class);
	}
}
