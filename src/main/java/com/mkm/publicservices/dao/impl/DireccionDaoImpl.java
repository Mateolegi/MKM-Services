package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.DireccionDao;
import com.mkm.publicservices.model.Direccion;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class DireccionDaoImpl extends CrudDaoImpl<Direccion> implements DireccionDao {
	
	public DireccionDaoImpl() {
		super(Direccion.class);
	}

}
