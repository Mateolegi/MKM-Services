package com.mkm.publicservices.dao.impl;

import java.util.Date;

import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.ValorConsumoDao;
import com.mkm.publicservices.model.TipoConsumo;
import com.mkm.publicservices.model.ValorConsumo;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;
import io.github.mateolegi.util.HibernateUtil;
import io.github.mateolegi.util.TransactionException;

public class ValorConsumoDaoImpl extends CrudDaoImpl<ValorConsumo> implements ValorConsumoDao {

	private static final Logger LOGGER = LogManager.getLogger(ValorConsumoDaoImpl.class);

	public ValorConsumoDaoImpl() {
		super(ValorConsumo.class);
	}

	@Override
	public ValorConsumo obtenerValorConsumoTipoAnno(TipoConsumo tipoConsumo, Date anno) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			ValorConsumo valorConsumo = entityManager.createNamedQuery("ValorConsumo.findByTipoAndYear", ValorConsumo.class)
				.setParameter("tipoConsumo", tipoConsumo)
				.setParameter("anno", anno)
				.getSingleResult();
			return valorConsumo;
		} catch (NoResultException e) {
			return null;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}
}
