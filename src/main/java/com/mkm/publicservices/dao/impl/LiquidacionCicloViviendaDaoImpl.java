package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.LiquidacionCicloViviendaDao;
import com.mkm.publicservices.model.LiquidacionCicloVivienda;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class LiquidacionCicloViviendaDaoImpl extends CrudDaoImpl<LiquidacionCicloVivienda> implements LiquidacionCicloViviendaDao {
	public LiquidacionCicloViviendaDaoImpl() {
		super(LiquidacionCicloVivienda.class);
	}
}
