package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.UsuarioDao;
import com.mkm.publicservices.model.Usuario;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class UsuarioDaoImpl extends CrudDaoImpl<Usuario> implements UsuarioDao {

	public UsuarioDaoImpl() {
		super(Usuario.class);
	}

	@Override
	public Usuario findByUsername(String username) {
		String jpql = "SELECT u FROM Usuario u WHERE u.nombreUsuario = ?1";
		return (Usuario) jpqlQuery(jpql, false, username);
	}

	@Override
	public Usuario findByEmail(String email) {
		String jpql = "SELECT u FROM Usuario u INNER JOIN u.persona p WHERE p.email = ?1";
		return (Usuario) jpqlQuery(jpql, false, email);
	}
}
