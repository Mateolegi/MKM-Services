package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.TipoViaDao;
import com.mkm.publicservices.model.TipoVia;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class TipoViaDaoImpl extends CrudDaoImpl<TipoVia> implements TipoViaDao {

	public TipoViaDaoImpl() {
		super(TipoVia.class);
	}
}
