package com.mkm.publicservices.dao.impl;

import java.util.List;

import com.mkm.publicservices.dao.PersonaDao;
import com.mkm.publicservices.model.Persona;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class PersonaDaoImpl extends CrudDaoImpl<Persona> implements PersonaDao {
	public PersonaDaoImpl() {
		super(Persona.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Persona> getPropietarios() {
		String jpql = "SELECT DISTINCT h.responsable FROM Hogar h";
		return (List<Persona>) jpqlQuery(jpql, true);
	}
}
