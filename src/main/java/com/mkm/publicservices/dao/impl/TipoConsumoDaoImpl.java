package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.TipoConsumoDao;
import com.mkm.publicservices.model.TipoConsumo;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class TipoConsumoDaoImpl extends CrudDaoImpl<TipoConsumo> implements TipoConsumoDao {

	public TipoConsumoDaoImpl() {
		super(TipoConsumo.class);
	}
}
