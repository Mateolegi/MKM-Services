package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.TipoDocumentoDao;
import com.mkm.publicservices.model.TipoDocumento;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class TipoDocumentoDaoImpl extends CrudDaoImpl<TipoDocumento> implements TipoDocumentoDao {

	public TipoDocumentoDaoImpl() {
		super(TipoDocumento.class);
	}
}
