package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.TipoPerfilDao;
import com.mkm.publicservices.model.TipoPerfil;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class TipoPerfilDaoImpl extends CrudDaoImpl<TipoPerfil> implements TipoPerfilDao {

	public TipoPerfilDaoImpl() {
		super(TipoPerfil.class);
		
	}

}
