package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.EstadoLiquidacionDao;
import com.mkm.publicservices.model.EstadoLiquidacion;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class EstadoLiquidacionDaoImpl extends CrudDaoImpl<EstadoLiquidacion> implements EstadoLiquidacionDao {
	public EstadoLiquidacionDaoImpl() {
		super(EstadoLiquidacion.class);
	}

}
