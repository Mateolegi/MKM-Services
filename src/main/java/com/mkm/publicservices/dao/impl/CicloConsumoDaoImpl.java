package com.mkm.publicservices.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.CicloConsumoDao;
import com.mkm.publicservices.model.CicloConsumo;
import com.mkm.publicservices.model.Hogar;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;
import io.github.mateolegi.util.HibernateUtil;
import io.github.mateolegi.util.TransactionException;

public class CicloConsumoDaoImpl extends CrudDaoImpl<CicloConsumo> implements CicloConsumoDao {

	private static final Logger LOGGER = LogManager.getLogger(CicloConsumoDaoImpl.class);

	public CicloConsumoDaoImpl() {
		super(CicloConsumo.class);
	}

	@Override
	public List<CicloConsumo> obtenerCiclosConsumoRango(Hogar hogar, Date fechaDesde, Date fechaHasta) {
		try {
			entityManager = HibernateUtil.createEntityManager();
			LOGGER.debug("Inicia una transacción");
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<CicloConsumo> query = builder.createQuery(CicloConsumo.class);
			Root<CicloConsumo> root = query.from(CicloConsumo.class);
			query.select(root);
			query.where(new Predicate[] {
				builder.equal(root.get("hogar"), hogar),
				builder.or(new Predicate[] {
					builder.between(root.get("fechaInicio"), fechaDesde, fechaHasta),
					builder.between(root.get("fechaFin"), fechaDesde, fechaHasta)
				})
			});
			TypedQuery<CicloConsumo> q = entityManager.createQuery(query);
			List<CicloConsumo> list = q.getResultList();
			LOGGER.debug("Finaliza la transacción");
			return list;
		} catch (RuntimeException e) {
			LOGGER.error("Error durante la transacción", e);
			throw e;
		} catch (Exception e) {
			LOGGER.error("Error durante la transacción", e);
			throw new TransactionException(e);
		} finally {
			closeSession();
		}
	}
}
