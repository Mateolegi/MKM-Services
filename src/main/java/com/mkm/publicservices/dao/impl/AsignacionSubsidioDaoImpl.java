package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.AsignacionSubsidioDao;
import com.mkm.publicservices.model.AsignacionSubsidio;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class AsignacionSubsidioDaoImpl extends CrudDaoImpl<AsignacionSubsidio> implements AsignacionSubsidioDao {

	public AsignacionSubsidioDaoImpl() {
		super(AsignacionSubsidio.class);
	}
}
