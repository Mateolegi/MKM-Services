package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.LiquidacionValorConsumoDao;
import com.mkm.publicservices.model.LiquidacionValorConsumo;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;

public class LiquidacionValorConsumoDaoImpl extends CrudDaoImpl<LiquidacionValorConsumo> implements LiquidacionValorConsumoDao{
	public LiquidacionValorConsumoDaoImpl() {
		super(LiquidacionValorConsumo.class);
	}
}
