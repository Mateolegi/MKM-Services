package com.mkm.publicservices.dao.impl;

import com.mkm.publicservices.dao.PromedioCicloConsumoDao;
import com.mkm.publicservices.model.PromedioConsumoCiclo;

import io.github.mateolegi.database.dao.impl.CrudDaoImpl;


public class PromedioCicloConsumoDaoImpl extends CrudDaoImpl<PromedioConsumoCiclo> implements PromedioCicloConsumoDao{

	public PromedioCicloConsumoDaoImpl() {
		super(PromedioConsumoCiclo.class);
	}
}
