package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.LiquidacionValorConsumo;

import io.github.mateolegi.database.dao.CrudDao;

public interface LiquidacionValorConsumoDao extends CrudDao<LiquidacionValorConsumo> {

}
