package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoEstadoLiquidacion;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoEstadoLiquidacionDao extends CrudDao<TipoEstadoLiquidacion> {

}
