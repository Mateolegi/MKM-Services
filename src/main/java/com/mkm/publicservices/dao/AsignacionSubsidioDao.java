package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.AsignacionSubsidio;

import io.github.mateolegi.database.dao.CrudDao;

public interface AsignacionSubsidioDao extends CrudDao<AsignacionSubsidio> {

}
