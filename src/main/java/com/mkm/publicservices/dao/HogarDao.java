package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.Hogar;

import io.github.mateolegi.database.dao.CrudDao;

public interface HogarDao extends CrudDao<Hogar> {

}
