package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoPerfil;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoPerfilDao extends CrudDao<TipoPerfil> {

}
