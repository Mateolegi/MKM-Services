package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.EstadoLiquidacion;

import io.github.mateolegi.database.dao.CrudDao;

public interface EstadoLiquidacionDao extends CrudDao<EstadoLiquidacion> {

}
