package com.mkm.publicservices.dao;

import com.mkm.publicservices.model.TipoConsumo;

import io.github.mateolegi.database.dao.CrudDao;

public interface TipoConsumoDao extends CrudDao<TipoConsumo> {

}
