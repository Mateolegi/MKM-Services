package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the tipo_perfil database table.
 * 
 */
@Entity
@Table(name="tipo_perfil")
@NamedQuery(name="TipoPerfil.findAll", query="SELECT t FROM TipoPerfil t")
public class TipoPerfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_PERFIL", unique=true, nullable=false)
	private int idTipoPerfil;

	@Expose
	@Column(name="TIPO_PERFIL", nullable=false, length=45)
	private String tipoPerfil;

	public int getIdTipoPerfil() {
		return this.idTipoPerfil;
	}

	public void setIdTipoPerfil(int idTipoPerfil) {
		this.idTipoPerfil = idTipoPerfil;
	}

	public String getTipoPerfil() {
		return this.tipoPerfil;
	}

	public void setTipoPerfil(String tipoPerfil) {
		this.tipoPerfil = tipoPerfil;
	}

}