package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the liquidacion_ciclo_vivienda database table.
 * 
 */
@Entity
@Table(name="liquidacion_ciclo_vivienda")
@NamedQuery(name="LiquidacionCicloVivienda.findAll", query="SELECT l FROM LiquidacionCicloVivienda l")
public class LiquidacionCicloVivienda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_LIQUIDACION_CICLO_VIVIENDA", unique=true, nullable=false)
	private int idLiquidacionCicloVivienda;

	@Expose
	//bi-directional many-to-one association to EstadoLiquidacion
	@OneToMany(mappedBy="liquidacionCicloVivienda")
	private List<EstadoLiquidacion> estadoLiquidaciones;

	@Expose
	//bi-directional many-to-one association to CicloConsumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CICLO_CONSUMO", nullable=false)
	private CicloConsumo cicloConsumo;

	@Expose
	//bi-directional many-to-one association to Hogar
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOGAR", nullable=false)
	private Hogar hogar;

	@Expose
	//bi-directional many-to-one association to LiquidacionValorConsumo
	@OneToMany(mappedBy="liquidacionCicloVivienda")
	private List<LiquidacionValorConsumo> liquidacionValorConsumos;

	public int getIdLiquidacionCicloVivienda() {
		return this.idLiquidacionCicloVivienda;
	}

	public void setIdLiquidacionCicloVivienda(int idLiquidacionCicloVivienda) {
		this.idLiquidacionCicloVivienda = idLiquidacionCicloVivienda;
	}

	public List<EstadoLiquidacion> getEstadoLiquidaciones() {
		return this.estadoLiquidaciones;
	}

	public void setEstadoLiquidaciones(List<EstadoLiquidacion> estadoLiquidaciones) {
		this.estadoLiquidaciones = estadoLiquidaciones;
	}

	public EstadoLiquidacion addEstadoLiquidacione(EstadoLiquidacion estadoLiquidacione) {
		getEstadoLiquidaciones().add(estadoLiquidacione);
		estadoLiquidacione.setLiquidacionCicloVivienda(this);

		return estadoLiquidacione;
	}

	public EstadoLiquidacion removeEstadoLiquidacione(EstadoLiquidacion estadoLiquidacione) {
		getEstadoLiquidaciones().remove(estadoLiquidacione);
		estadoLiquidacione.setLiquidacionCicloVivienda(null);

		return estadoLiquidacione;
	}

	public CicloConsumo getCicloConsumo() {
		return this.cicloConsumo;
	}

	public void setCicloConsumo(CicloConsumo cicloConsumo) {
		this.cicloConsumo = cicloConsumo;
	}

	public Hogar getHogar() {
		return this.hogar;
	}

	public void setHogar(Hogar hogar) {
		this.hogar = hogar;
	}

	public List<LiquidacionValorConsumo> getLiquidacionValorConsumos() {
		return this.liquidacionValorConsumos;
	}

	public void setLiquidacionValorConsumos(List<LiquidacionValorConsumo> liquidacionValorConsumos) {
		this.liquidacionValorConsumos = liquidacionValorConsumos;
	}

	public LiquidacionValorConsumo addLiquidacionValorConsumo(LiquidacionValorConsumo liquidacionValorConsumo) {
		getLiquidacionValorConsumos().add(liquidacionValorConsumo);
		liquidacionValorConsumo.setLiquidacionCicloVivienda(this);

		return liquidacionValorConsumo;
	}

	public LiquidacionValorConsumo removeLiquidacionValorConsumo(LiquidacionValorConsumo liquidacionValorConsumo) {
		getLiquidacionValorConsumos().remove(liquidacionValorConsumo);
		liquidacionValorConsumo.setLiquidacionCicloVivienda(null);

		return liquidacionValorConsumo;
	}

}