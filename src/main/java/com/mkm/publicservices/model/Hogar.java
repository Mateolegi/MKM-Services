package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the hogar database table.
 * 
 */
@Entity
@Table(name="hogar")
@NamedQuery(name="Hogar.findAll", query="SELECT h FROM Hogar h")
public class Hogar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_HOGAR", unique=true, nullable=false)
	private int idHogar;

	@Expose
	@Column(name="ESTRATO_SOCIOECONOMICO", nullable=false)
	private int estratoSocioeconomico;

	@Expose
	@Column(nullable=false, length=20)
	private String telefono;

	//bi-directional many-to-one association to AsignacionSubsidio
	@OneToMany(mappedBy="hogar")
	private List<AsignacionSubsidio> asignacionSubsidios;

	//bi-directional many-to-one association to CicloConsumo
	@OneToMany(mappedBy="hogar")
	private List<CicloConsumo> cicloConsumos;

	@Expose
	//bi-directional one-to-one association to Direccion
	@OneToOne(cascade={CascadeType.REMOVE}, fetch=FetchType.EAGER)
	@JoinColumn(name="ID_DIRECCION", nullable=false)
	private Direccion direccion;

	@Expose
	//bi-directional many-to-one association to Persona
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RESPONSABLE", nullable=false)
	private Persona responsable;

	@Expose
	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO_REGISTRA", nullable=false)
	private Usuario usuario;

	//bi-directional many-to-one association to LiquidacionCicloVivienda
	@OneToMany(mappedBy="hogar")
	private List<LiquidacionCicloVivienda> liquidacionCicloViviendas;

	public int getIdHogar() {
		return this.idHogar;
	}

	public void setIdHogar(int idHogar) {
		this.idHogar = idHogar;
	}

	public int getEstratoSocioeconomico() {
		return this.estratoSocioeconomico;
	}

	public void setEstratoSocioeconomico(int estratoSocioeconomico) {
		this.estratoSocioeconomico = estratoSocioeconomico;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<AsignacionSubsidio> getAsignacionSubsidios() {
		return this.asignacionSubsidios;
	}

	public void setAsignacionSubsidios(List<AsignacionSubsidio> asignacionSubsidios) {
		this.asignacionSubsidios = asignacionSubsidios;
	}

	public AsignacionSubsidio addAsignacionSubsidio(AsignacionSubsidio asignacionSubsidio) {
		getAsignacionSubsidios().add(asignacionSubsidio);
		asignacionSubsidio.setHogar(this);

		return asignacionSubsidio;
	}

	public AsignacionSubsidio removeAsignacionSubsidio(AsignacionSubsidio asignacionSubsidio) {
		getAsignacionSubsidios().remove(asignacionSubsidio);
		asignacionSubsidio.setHogar(null);

		return asignacionSubsidio;
	}

	public List<CicloConsumo> getCicloConsumos() {
		return this.cicloConsumos;
	}

	public void setCicloConsumos(List<CicloConsumo> cicloConsumos) {
		this.cicloConsumos = cicloConsumos;
	}

	public CicloConsumo addCicloConsumo(CicloConsumo cicloConsumo) {
		getCicloConsumos().add(cicloConsumo);
		cicloConsumo.setHogar(this);

		return cicloConsumo;
	}

	public CicloConsumo removeCicloConsumo(CicloConsumo cicloConsumo) {
		getCicloConsumos().remove(cicloConsumo);
		cicloConsumo.setHogar(null);

		return cicloConsumo;
	}

	public Direccion getDireccion() {
		return this.direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public Persona getResponsable() {
		return this.responsable;
	}

	public void setResponsable(Persona responsable) {
		this.responsable = responsable;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<LiquidacionCicloVivienda> getLiquidacionCicloViviendas() {
		return this.liquidacionCicloViviendas;
	}

	public void setLiquidacionCicloViviendas(List<LiquidacionCicloVivienda> liquidacionCicloViviendas) {
		this.liquidacionCicloViviendas = liquidacionCicloViviendas;
	}

	public LiquidacionCicloVivienda addLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		getLiquidacionCicloViviendas().add(liquidacionCicloVivienda);
		liquidacionCicloVivienda.setHogar(this);

		return liquidacionCicloVivienda;
	}

	public LiquidacionCicloVivienda removeLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		getLiquidacionCicloViviendas().remove(liquidacionCicloVivienda);
		liquidacionCicloVivienda.setHogar(null);

		return liquidacionCicloVivienda;
	}

}