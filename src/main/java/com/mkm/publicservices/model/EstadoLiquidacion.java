package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.sql.Timestamp;


/**
 * The persistent class for the estado_liquidacion database table.
 * 
 */
@Entity
@Table(name="estado_liquidacion")
@NamedQuery(name="EstadoLiquidacion.findAll", query="SELECT e FROM EstadoLiquidacion e")
public class EstadoLiquidacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ESTADO_LIQUIDACION", unique=true, nullable=false)
	private int idEstadoLiquidacion;

	@Expose
	@Column(name="FECHA_REGISTRO", nullable=false)
	private Timestamp fechaRegistro;

	//bi-directional many-to-one association to LiquidacionCicloVivienda
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LIQUIDACION_CICLO_VIVIENDA", nullable=false)
	private LiquidacionCicloVivienda liquidacionCicloVivienda;

	@Expose
	//uni-directional many-to-one association to TipoEstadoLiquidacion
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_ESTADO_LIQUIDACION", nullable=false)
	private TipoEstadoLiquidacion tipoEstadoLiquidacion;

	@Expose
	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO_RESPONSABLE", nullable=false)
	private Usuario usuario;

	@Expose
	@JoinColumn(name="ACTIVO", nullable=true)
	private byte activo;

	public byte getActivo() {
		return activo;
	}

	public void setActivo(byte activo) {
		this.activo = activo;
	}

	public int getIdEstadoLiquidacion() {
		return this.idEstadoLiquidacion;
	}

	public void setIdEstadoLiquidacion(int idEstadoLiquidacion) {
		this.idEstadoLiquidacion = idEstadoLiquidacion;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public LiquidacionCicloVivienda getLiquidacionCicloVivienda() {
		return this.liquidacionCicloVivienda;
	}

	public void setLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		this.liquidacionCicloVivienda = liquidacionCicloVivienda;
	}

	public TipoEstadoLiquidacion getTipoEstadoLiquidacion() {
		return this.tipoEstadoLiquidacion;
	}

	public void setTipoEstadoLiquidacion(TipoEstadoLiquidacion tipoEstadoLiquidacion) {
		this.tipoEstadoLiquidacion = tipoEstadoLiquidacion;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}