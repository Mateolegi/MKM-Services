package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@Table(name="persona")
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERSONA", unique=true, nullable=false)
	private int idPersona;

	@Expose
	@Column(nullable=false, length=45)
	private String apellido;

	@Expose
	@Column(length=60)
	private String email;

	@Expose
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_NACIMIENTO", nullable=false)
	private Date fechaNacimiento;

	@Expose
	@Column(name="FECHA_REGISTRO", nullable=false)
	private Timestamp fechaRegistro;

	@Expose
	@Column(nullable=false)
	private byte genero;

	@Expose
	@Column(nullable=false, length=45)
	private String nombre;

	@Expose
	@Column(name="NUMERO_DOCUMENTO", nullable=false, length=20)
	private String numeroDocumento;

	//bi-directional many-to-one association to Hogar
	@OneToMany(mappedBy="responsable")
	private List<Hogar> hogares;

	@Expose
	//uni-directional many-to-one association to TipoDocumento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_DOCUMENTO", nullable=false)
	private TipoDocumento tipoDocumento;

	//bi-directional one-to-one association to Usuario
	@OneToOne(mappedBy="persona", fetch=FetchType.LAZY)
	private Usuario usuario;

	public int getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public byte getGenero() {
		return this.genero;
	}

	public void setGenero(byte genero) {
		this.genero = genero;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public List<Hogar> getHogares() {
		return this.hogares;
	}

	public void setHogares(List<Hogar> hogares) {
		this.hogares = hogares;
	}

	public Hogar addHogare(Hogar hogare) {
		getHogares().add(hogare);
		hogare.setResponsable(this);

		return hogare;
	}

	public Hogar removeHogare(Hogar hogare) {
		getHogares().remove(hogare);
		hogare.setResponsable(null);

		return hogare;
	}

	public TipoDocumento getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}