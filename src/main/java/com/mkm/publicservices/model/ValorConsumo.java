package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the valor_consumo database table.
 * 
 */
@Entity
@Table(name="valor_consumo")
@NamedQueries({
	@NamedQuery(name="ValorConsumo.findAll", query="SELECT v FROM ValorConsumo v"),
	@NamedQuery(name="ValorConsumo.findByTipoAndYear", query="SELECT v FROM ValorConsumo v WHERE v.tipoConsumo = :tipoConsumo AND v.anno = :anno")
})
public class ValorConsumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_VALOR_CONSUMO", unique=true, nullable=false)
	private int idValorConsumo;

	@Expose
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date anno;

	@Expose
	@Column(name="VALOR_CONSUMO", nullable=false, precision=10, scale=4)
	private BigDecimal valorConsumo;

	@Expose
	//uni-directional many-to-one association to TipoConsumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_CONSUMO", nullable=false)
	private TipoConsumo tipoConsumo;

	public int getIdValorConsumo() {
		return this.idValorConsumo;
	}

	public void setIdValorConsumo(int idValorConsumo) {
		this.idValorConsumo = idValorConsumo;
	}

	public Date getAnno() {
		return this.anno;
	}

	public void setAnno(Date anno) {
		this.anno = anno;
	}

	public BigDecimal getValorConsumo() {
		return this.valorConsumo;
	}

	public void setValorConsumo(BigDecimal valorConsumo) {
		this.valorConsumo = valorConsumo;
	}

	public TipoConsumo getTipoConsumo() {
		return this.tipoConsumo;
	}

	public void setTipoConsumo(TipoConsumo tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

}