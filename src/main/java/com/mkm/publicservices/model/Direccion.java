package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the direccion database table.
 * 
 */
@Entity
@Table(name="direccion")
@NamedQuery(name="Direccion.findAll", query="SELECT d FROM Direccion d")
public class Direccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_DIRECCION", unique=true, nullable=false)
	private int idDireccion;

	@Expose
	@Column(nullable=false)
	private int complemento;

	@Expose
	@Column(name="CUADRANTE_GENERADORA", length=1)
	private String cuadranteGeneradora;

	@Expose
	@Column(name="CUADRANTE_PRINCIPAL", length=1)
	private String cuadrantePrincipal;

	@Expose
	@Column(length=20)
	private String interior;

	@Expose
	@Column(name="LETRA_GENERADORA", length=1)
	private String letraGeneradora;

	@Expose
	@Column(name="LETRA_PRINCIPAL", length=1)
	private String letraPrincipal;

	@Expose
	@Column(name="LETRA_SUFIJO", length=1)
	private String letraSufijo;

	@Expose
	@Column(name="LETRA_SUFIJO_GENERADORA", length=1)
	private String letraSufijoGeneradora;

	@Expose
	@Column(name="NUMERO_VIA_GENERADORA", nullable=false)
	private int numeroViaGeneradora;

	@Expose
	@Column(name="NUMERO_VIA_PRINCIPAL", nullable=false)
	private int numeroViaPrincipal;

	@Expose
	@Column(length=100)
	private String observacion;

	@Expose
	//uni-directional many-to-one association to TipoVia
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_VIA_PRINCIPAL", nullable=false)
	private TipoVia tipoVia;

	//bi-directional one-to-one association to Hogar
	@OneToOne(mappedBy="direccion", fetch=FetchType.LAZY)
	private Hogar hogar;

	public int getIdDireccion() {
		return this.idDireccion;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public int getComplemento() {
		return this.complemento;
	}

	public void setComplemento(int complemento) {
		this.complemento = complemento;
	}

	public String getCuadranteGeneradora() {
		return this.cuadranteGeneradora;
	}

	public void setCuadranteGeneradora(String cuadranteGeneradora) {
		this.cuadranteGeneradora = cuadranteGeneradora;
	}

	public String getCuadrantePrincipal() {
		return this.cuadrantePrincipal;
	}

	public void setCuadrantePrincipal(String cuadrantePrincipal) {
		this.cuadrantePrincipal = cuadrantePrincipal;
	}

	public String getInterior() {
		return this.interior;
	}

	public void setInterior(String interior) {
		this.interior = interior;
	}

	public String getLetraGeneradora() {
		return this.letraGeneradora;
	}

	public void setLetraGeneradora(String letraGeneradora) {
		this.letraGeneradora = letraGeneradora;
	}

	public String getLetraPrincipal() {
		return this.letraPrincipal;
	}

	public void setLetraPrincipal(String letraPrincipal) {
		this.letraPrincipal = letraPrincipal;
	}

	public String getLetraSufijo() {
		return this.letraSufijo;
	}

	public void setLetraSufijo(String letraSufijo) {
		this.letraSufijo = letraSufijo;
	}

	public String getLetraSufijoGeneradora() {
		return this.letraSufijoGeneradora;
	}

	public void setLetraSufijoGeneradora(String letraSufijoGeneradora) {
		this.letraSufijoGeneradora = letraSufijoGeneradora;
	}

	public int getNumeroViaGeneradora() {
		return this.numeroViaGeneradora;
	}

	public void setNumeroViaGeneradora(int numeroViaGeneradora) {
		this.numeroViaGeneradora = numeroViaGeneradora;
	}

	public int getNumeroViaPrincipal() {
		return this.numeroViaPrincipal;
	}

	public void setNumeroViaPrincipal(int numeroViaPrincipal) {
		this.numeroViaPrincipal = numeroViaPrincipal;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public TipoVia getTipoVia() {
		return this.tipoVia;
	}

	public void setTipoVia(TipoVia tipoVia) {
		this.tipoVia = tipoVia;
	}

	public Hogar getHogar() {
		return this.hogar;
	}

	public void setHogar(Hogar hogar) {
		this.hogar = hogar;
	}

}