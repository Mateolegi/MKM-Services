package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the promedio_consumo_ciclo database table.
 * 
 */
@Entity
@Table(name="promedio_consumo_ciclo")
@NamedQuery(name="PromedioConsumoCiclo.findAll", query="SELECT p FROM PromedioConsumoCiclo p")
public class PromedioConsumoCiclo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PROMEDIO_CONSUMO_CICLO", unique=true, nullable=false)
	private int idPromedioConsumoCiclo;

	@Expose
	@Column(nullable=false)
	private double promedio;

	@Expose
	//bi-directional many-to-one association to AsignacionSubsidio
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ASIGNACION_SUBSIDIO", nullable=false)
	private AsignacionSubsidio asignacionSubsidio;

	@Expose
	//uni-directional many-to-one association to TipoConsumo
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_CONSUMO", nullable=false)
	private TipoConsumo tipoConsumo;

	public int getIdPromedioConsumoCiclo() {
		return this.idPromedioConsumoCiclo;
	}

	public void setIdPromedioConsumoCiclo(int idPromedioConsumoCiclo) {
		this.idPromedioConsumoCiclo = idPromedioConsumoCiclo;
	}

	public double getPromedio() {
		return this.promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	public AsignacionSubsidio getAsignacionSubsidio() {
		return this.asignacionSubsidio;
	}

	public void setAsignacionSubsidio(AsignacionSubsidio asignacionSubsidio) {
		this.asignacionSubsidio = asignacionSubsidio;
	}

	public TipoConsumo getTipoConsumo() {
		return this.tipoConsumo;
	}

	public void setTipoConsumo(TipoConsumo tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

}