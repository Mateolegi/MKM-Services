package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the tipo_estado_liquidacion database table.
 * 
 */
@Entity
@Table(name="tipo_estado_liquidacion")
@NamedQuery(name="TipoEstadoLiquidacion.findAll", query="SELECT t FROM TipoEstadoLiquidacion t")
public class TipoEstadoLiquidacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_ESTADO_LIQUIDACION", unique=true, nullable=false)
	private int idTipoEstadoLiquidacion;

	@Expose
	@Column(name="TIPO_ESTADO_LIQUIDACION", nullable=false, length=45)
	private String tipoEstadoLiquidacion;

	public int getIdTipoEstadoLiquidacion() {
		return this.idTipoEstadoLiquidacion;
	}

	public void setIdTipoEstadoLiquidacion(int idTipoEstadoLiquidacion) {
		this.idTipoEstadoLiquidacion = idTipoEstadoLiquidacion;
	}

	public String getTipoEstadoLiquidacion() {
		return this.tipoEstadoLiquidacion;
	}

	public void setTipoEstadoLiquidacion(String tipoEstadoLiquidacion) {
		this.tipoEstadoLiquidacion = tipoEstadoLiquidacion;
	}

}