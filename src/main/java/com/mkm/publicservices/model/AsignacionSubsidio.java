package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the asignacion_subsidio database table.
 * 
 */
@Entity
@Table(name="asignacion_subsidio")
@NamedQuery(name="AsignacionSubsidio.findAll", query="SELECT a FROM AsignacionSubsidio a")
public class AsignacionSubsidio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ASIGNACION_SUBSIDIO", unique=true, nullable=false)
	private int idAsignacionSubsidio;

	//bi-directional many-to-one association to CicloConsumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CICLO_CONSUMO", nullable=false)
	private CicloConsumo cicloConsumo;

	//bi-directional many-to-one association to Hogar
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOGAR", nullable=false)
	private Hogar hogar;

	//bi-directional many-to-one association to PromedioConsumoCiclo
	@OneToMany(mappedBy="asignacionSubsidio")
	private List<PromedioConsumoCiclo> promedioConsumoCiclos;

	public int getIdAsignacionSubsidio() {
		return this.idAsignacionSubsidio;
	}

	public void setIdAsignacionSubsidio(int idAsignacionSubsidio) {
		this.idAsignacionSubsidio = idAsignacionSubsidio;
	}

	public CicloConsumo getCicloConsumo() {
		return this.cicloConsumo;
	}

	public void setCicloConsumo(CicloConsumo cicloConsumo) {
		this.cicloConsumo = cicloConsumo;
	}

	public Hogar getHogar() {
		return this.hogar;
	}

	public void setHogar(Hogar hogar) {
		this.hogar = hogar;
	}

	public List<PromedioConsumoCiclo> getPromedioConsumoCiclos() {
		return this.promedioConsumoCiclos;
	}

	public void setPromedioConsumoCiclos(List<PromedioConsumoCiclo> promedioConsumoCiclos) {
		this.promedioConsumoCiclos = promedioConsumoCiclos;
	}

	public PromedioConsumoCiclo addPromedioConsumoCiclo(PromedioConsumoCiclo promedioConsumoCiclo) {
		getPromedioConsumoCiclos().add(promedioConsumoCiclo);
		promedioConsumoCiclo.setAsignacionSubsidio(this);

		return promedioConsumoCiclo;
	}

	public PromedioConsumoCiclo removePromedioConsumoCiclo(PromedioConsumoCiclo promedioConsumoCiclo) {
		getPromedioConsumoCiclos().remove(promedioConsumoCiclo);
		promedioConsumoCiclo.setAsignacionSubsidio(null);

		return promedioConsumoCiclo;
	}

}