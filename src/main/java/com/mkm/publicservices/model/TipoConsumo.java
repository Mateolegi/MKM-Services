package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the tipo_consumo database table.
 * 
 */
@Entity
@Table(name="tipo_consumo")
@NamedQuery(name="TipoConsumo.findAll", query="SELECT t FROM TipoConsumo t")
public class TipoConsumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_CONSUMO", unique=true, nullable=false)
	private int idTipoConsumo;

	@Expose
	@Column(name="TIPO_CONSUMO", nullable=false, length=45)
	private String tipoConsumo;

	public int getIdTipoConsumo() {
		return this.idTipoConsumo;
	}

	public void setIdTipoConsumo(int idTipoConsumo) {
		this.idTipoConsumo = idTipoConsumo;
	}

	public String getTipoConsumo() {
		return this.tipoConsumo;
	}

	public void setTipoConsumo(String tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

}