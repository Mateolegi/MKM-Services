package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@Table(name="usuario")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_USUARIO", unique=true, nullable=false)
	private int idUsuario;

	@Expose
	@Column(nullable=false, length=300)
	private String contrasena;

	@Expose
	@Column(nullable=false)
	private byte estado;

	//@Expose
	@Column(name="FECHA_CREACION")
	private Timestamp fechaCreacion;

	@Expose
	@Column(name="NOMBRE_USUARIO", nullable=false, length=20)
	private String nombreUsuario;

	//bi-directional many-to-one association to EstadoLiquidacion
	@OneToMany(mappedBy="usuario")
	private List<EstadoLiquidacion> estadoLiquidaciones;

	//bi-directional many-to-one association to Hogar
	@OneToMany(mappedBy="usuario")
	private List<Hogar> hogares;

	@Expose
	//bi-directional one-to-one association to Persona
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PERSONA", nullable=false)
	private Persona persona;

	@Expose
	//uni-directional many-to-many association to TipoPerfil
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
		name="usuario_perfil"
		, joinColumns={
			@JoinColumn(name="ID_USUARIO", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_TIPO_PERFIL", nullable=false)
			}
		)
	private List<TipoPerfil> tipoPerfiles;

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getContrasena() {
		return this.contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public byte getEstado() {
		return this.estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<EstadoLiquidacion> getEstadoLiquidaciones() {
		return this.estadoLiquidaciones;
	}

	public void setEstadoLiquidaciones(List<EstadoLiquidacion> estadoLiquidaciones) {
		this.estadoLiquidaciones = estadoLiquidaciones;
	}

	public EstadoLiquidacion addEstadoLiquidacione(EstadoLiquidacion estadoLiquidacione) {
		getEstadoLiquidaciones().add(estadoLiquidacione);
		estadoLiquidacione.setUsuario(this);

		return estadoLiquidacione;
	}

	public EstadoLiquidacion removeEstadoLiquidacione(EstadoLiquidacion estadoLiquidacione) {
		getEstadoLiquidaciones().remove(estadoLiquidacione);
		estadoLiquidacione.setUsuario(null);

		return estadoLiquidacione;
	}

	public List<Hogar> getHogares() {
		return this.hogares;
	}

	public void setHogares(List<Hogar> hogares) {
		this.hogares = hogares;
	}

	public Hogar addHogare(Hogar hogare) {
		getHogares().add(hogare);
		hogare.setUsuario(this);

		return hogare;
	}

	public Hogar removeHogare(Hogar hogare) {
		getHogares().remove(hogare);
		hogare.setUsuario(null);

		return hogare;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<TipoPerfil> getTipoPerfiles() {
		return this.tipoPerfiles;
	}

	public void setTipoPerfiles(List<TipoPerfil> tipoPerfiles) {
		this.tipoPerfiles = tipoPerfiles;
	}

}