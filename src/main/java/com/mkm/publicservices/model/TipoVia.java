package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the tipo_via database table.
 * 
 */
@Entity
@Table(name="tipo_via")
@NamedQuery(name="TipoVia.findAll", query="SELECT t FROM TipoVia t")
public class TipoVia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_VIA", unique=true, nullable=false)
	private int idTipoVia;

	@Expose
	@Column(nullable=false, length=5)
	private String abreviacion;

	@Expose
	@Column(name="TIPO_VIA", nullable=false, length=45)
	private String tipoVia;

	public int getIdTipoVia() {
		return this.idTipoVia;
	}

	public void setIdTipoVia(int idTipoVia) {
		this.idTipoVia = idTipoVia;
	}

	public String getAbreviacion() {
		return this.abreviacion;
	}

	public void setAbreviacion(String abreviacion) {
		this.abreviacion = abreviacion;
	}

	public String getTipoVia() {
		return this.tipoVia;
	}

	public void setTipoVia(String tipoVia) {
		this.tipoVia = tipoVia;
	}

}