package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;


/**
 * The persistent class for the tipo_parametro database table.
 * 
 */
@Entity
@Table(name="tipo_parametro")
@NamedQueries({
	@NamedQuery(name="TipoParametro.findAll", query="SELECT t FROM TipoParametro t"),
	@NamedQuery(name="TipoParametro.findByName", query="SELECT t FROM TipoParametro t WHERE t.tipoParametro = :tipoParametro")
})
public class TipoParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_PARAMETRO", unique=true, nullable=false)
	private int idTipoParametro;

	@Expose
	@Column(nullable=false, length=300)
	private String descripcion;

	@Expose
	@Column(name="TIPO_PARAMETRO", nullable=false, length=45)
	private String tipoParametro;

	@Expose
	@Column(name="VALOR_PARAMETRO", nullable=false, length=200)
	private String valorParametro;

	public int getIdTipoParametro() {
		return this.idTipoParametro;
	}

	public void setIdTipoParametro(int idTipoParametro) {
		this.idTipoParametro = idTipoParametro;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoParametro() {
		return this.tipoParametro;
	}

	public void setTipoParametro(String tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	public String getValorParametro() {
		return this.valorParametro;
	}

	public void setValorParametro(String valorParametro) {
		this.valorParametro = valorParametro;
	}

}