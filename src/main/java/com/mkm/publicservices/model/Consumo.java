package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.List;


/**
 * The persistent class for the consumo database table.
 * 
 */
@Entity
@Table(name="consumo")
@NamedQuery(name="Consumo.findAll", query="SELECT c FROM Consumo c")
public class Consumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CONSUMO", unique=true, nullable=false)
	private int idConsumo;

	@Expose
	@Column(nullable=false)
	private double consumo;

	@Expose
	//bi-directional many-to-one association to CicloConsumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CICLO_CONSUMO", nullable=false)
	private CicloConsumo cicloConsumo;

	@Expose
	//uni-directional many-to-one association to TipoConsumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_CONSUMO", nullable=false)
	private TipoConsumo tipoConsumo;

	//bi-directional many-to-one association to LiquidacionValorConsumo
	@OneToMany(mappedBy="consumo")
	private List<LiquidacionValorConsumo> liquidacionValorConsumos;

	public int getIdConsumo() {
		return this.idConsumo;
	}

	public void setIdConsumo(int idConsumo) {
		this.idConsumo = idConsumo;
	}

	public double getConsumo() {
		return this.consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}

	public CicloConsumo getCicloConsumo() {
		return this.cicloConsumo;
	}

	public void setCicloConsumo(CicloConsumo cicloConsumo) {
		this.cicloConsumo = cicloConsumo;
	}

	public TipoConsumo getTipoConsumo() {
		return this.tipoConsumo;
	}

	public void setTipoConsumo(TipoConsumo tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

	public List<LiquidacionValorConsumo> getLiquidacionValorConsumos() {
		return this.liquidacionValorConsumos;
	}

	public void setLiquidacionValorConsumos(List<LiquidacionValorConsumo> liquidacionValorConsumos) {
		this.liquidacionValorConsumos = liquidacionValorConsumos;
	}

	public LiquidacionValorConsumo addLiquidacionValorConsumo(LiquidacionValorConsumo liquidacionValorConsumo) {
		getLiquidacionValorConsumos().add(liquidacionValorConsumo);
		liquidacionValorConsumo.setConsumo(this);

		return liquidacionValorConsumo;
	}

	public LiquidacionValorConsumo removeLiquidacionValorConsumo(LiquidacionValorConsumo liquidacionValorConsumo) {
		getLiquidacionValorConsumos().remove(liquidacionValorConsumo);
		liquidacionValorConsumo.setConsumo(null);

		return liquidacionValorConsumo;
	}

}