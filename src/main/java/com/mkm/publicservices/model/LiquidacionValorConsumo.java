package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;


/**
 * The persistent class for the liquidacion_valor_consumo database table.
 * 
 */
@Entity
@Table(name="liquidacion_valor_consumo")
@NamedQuery(name="LiquidacionValorConsumo.findAll", query="SELECT l FROM LiquidacionValorConsumo l")
public class LiquidacionValorConsumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_LIQUIDACION_VALOR_CONSUMO", unique=true, nullable=false)
	private int idLiquidacionValorConsumo;

	@Expose
	@Column(name="APLICA_SUBSIDIO", nullable=false)
	private byte aplicaSubsidio;

	@Expose
	@Column(name="VALOR_CONSUMO", nullable=false, precision=10, scale=4)
	private BigDecimal valorConsumo;

	@Expose
	//bi-directional many-to-one association to Consumo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CONSUMO", nullable=false)
	private Consumo consumo;

	//bi-directional many-to-one association to LiquidacionCicloVivienda
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LIQUIDACION_CICLO_VIVIENDA", nullable=false)
	private LiquidacionCicloVivienda liquidacionCicloVivienda;

	public int getIdLiquidacionValorConsumo() {
		return this.idLiquidacionValorConsumo;
	}

	public void setIdLiquidacionValorConsumo(int idLiquidacionValorConsumo) {
		this.idLiquidacionValorConsumo = idLiquidacionValorConsumo;
	}

	public byte getAplicaSubsidio() {
		return this.aplicaSubsidio;
	}

	public void setAplicaSubsidio(byte aplicaSubsidio) {
		this.aplicaSubsidio = aplicaSubsidio;
	}

	public BigDecimal getValorConsumo() {
		return this.valorConsumo;
	}

	public void setValorConsumo(BigDecimal valorConsumo) {
		this.valorConsumo = valorConsumo;
	}

	public Consumo getConsumo() {
		return this.consumo;
	}

	public void setConsumo(Consumo consumo) {
		this.consumo = consumo;
	}

	public LiquidacionCicloVivienda getLiquidacionCicloVivienda() {
		return this.liquidacionCicloVivienda;
	}

	public void setLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		this.liquidacionCicloVivienda = liquidacionCicloVivienda;
	}

}