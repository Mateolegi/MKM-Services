package com.mkm.publicservices.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ciclo_consumo database table.
 * 
 */
@Entity
@Table(name="ciclo_consumo")
@NamedQuery(name="CicloConsumo.findAll", query="SELECT c FROM CicloConsumo c")
public class CicloConsumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Expose
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CICLO_CONSUMO", unique=true, nullable=false)
	private int idCicloConsumo;

	@Expose
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FIN", nullable=false)
	private Date fechaFin;

	@Expose
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO", nullable=false)
	private Date fechaInicio;

	@Expose
	//bi-directional many-to-one association to AsignacionSubsidio
	@OneToMany(mappedBy="cicloConsumo")
	private List<AsignacionSubsidio> asignacionSubsidios;

	//bi-directional many-to-one association to Hogar
	@Expose
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_HOGAR", nullable=false)
	private Hogar hogar;

	//bi-directional many-to-one association to Consumo
	@OneToMany(mappedBy="cicloConsumo")
	private List<Consumo> consumos;

	//bi-directional many-to-one association to LiquidacionCicloVivienda
	@OneToMany(mappedBy="cicloConsumo")
	private List<LiquidacionCicloVivienda> liquidacionCicloViviendas;

	public int getIdCicloConsumo() {
		return this.idCicloConsumo;
	}

	public void setIdCicloConsumo(int idCicloConsumo) {
		this.idCicloConsumo = idCicloConsumo;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public List<AsignacionSubsidio> getAsignacionSubsidios() {
		return this.asignacionSubsidios;
	}

	public void setAsignacionSubsidios(List<AsignacionSubsidio> asignacionSubsidios) {
		this.asignacionSubsidios = asignacionSubsidios;
	}

	public AsignacionSubsidio addAsignacionSubsidio(AsignacionSubsidio asignacionSubsidio) {
		getAsignacionSubsidios().add(asignacionSubsidio);
		asignacionSubsidio.setCicloConsumo(this);

		return asignacionSubsidio;
	}

	public AsignacionSubsidio removeAsignacionSubsidio(AsignacionSubsidio asignacionSubsidio) {
		getAsignacionSubsidios().remove(asignacionSubsidio);
		asignacionSubsidio.setCicloConsumo(null);

		return asignacionSubsidio;
	}

	public Hogar getHogar() {
		return this.hogar;
	}

	public void setHogar(Hogar hogar) {
		this.hogar = hogar;
	}

	public List<Consumo> getConsumos() {
		return this.consumos;
	}

	public void setConsumos(List<Consumo> consumos) {
		this.consumos = consumos;
	}

	public Consumo addConsumo(Consumo consumo) {
		getConsumos().add(consumo);
		consumo.setCicloConsumo(this);

		return consumo;
	}

	public Consumo removeConsumo(Consumo consumo) {
		getConsumos().remove(consumo);
		consumo.setCicloConsumo(null);

		return consumo;
	}

	public List<LiquidacionCicloVivienda> getLiquidacionCicloViviendas() {
		return this.liquidacionCicloViviendas;
	}

	public void setLiquidacionCicloViviendas(List<LiquidacionCicloVivienda> liquidacionCicloViviendas) {
		this.liquidacionCicloViviendas = liquidacionCicloViviendas;
	}

	public LiquidacionCicloVivienda addLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		getLiquidacionCicloViviendas().add(liquidacionCicloVivienda);
		liquidacionCicloVivienda.setCicloConsumo(this);

		return liquidacionCicloVivienda;
	}

	public LiquidacionCicloVivienda removeLiquidacionCicloVivienda(LiquidacionCicloVivienda liquidacionCicloVivienda) {
		getLiquidacionCicloViviendas().remove(liquidacionCicloVivienda);
		liquidacionCicloVivienda.setCicloConsumo(null);

		return liquidacionCicloVivienda;
	}

}