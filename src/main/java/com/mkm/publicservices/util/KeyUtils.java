package com.mkm.publicservices.util;

import static io.jsonwebtoken.SignatureAlgorithm.HS512;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

public class KeyUtils {

	public static final String PUBLIC_KEY = "";
	public static final String PRIVATE_KEY = "MlTC7nYf2dPOBFgYAVqMwuRAYwkru5V5";
	public static final Key KEY = new SecretKeySpec(PRIVATE_KEY.getBytes(), HS512.getJcaName());
	
	private KeyUtils() { }
}
