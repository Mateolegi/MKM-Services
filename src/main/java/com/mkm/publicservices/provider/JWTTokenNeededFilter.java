package com.mkm.publicservices.provider;

import static javax.ws.rs.Priorities.AUTHENTICATION;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.glassfish.jersey.Severity.WARNING;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.annotation.JWTTokenNeeded;
import com.mkm.publicservices.util.ErrorMessage;
import com.mkm.publicservices.util.KeyUtils;

import io.jsonwebtoken.Jwts;

@Provider
@JWTTokenNeeded
@Priority(AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LogManager.getLogger(JWTTokenNeededFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext) {
		ErrorMessage errorMessage = new ErrorMessage();
    	errorMessage.setErrorCode(UNAUTHORIZED);
    	errorMessage.setErrorMessage("No se encontró un token de autentificación");
    	errorMessage.setSeverity(WARNING);
		// Obtiene el authorization header de la solicitud
        String authorizationHeader = requestContext.getHeaderString(AUTHORIZATION);
        LOGGER.info("Authorization Header: " + authorizationHeader);
        // Revisa si el HTTP Authorization header está presente y en un formato correcto
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
        	LOGGER.warn("Authorization Header inválido: " + authorizationHeader);
            requestContext.abortWith(Response.status(UNAUTHORIZED).entity(errorMessage).build());
            return;
        }
        // Extrae el token de la HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();
        LOGGER.info(requestContext.getUriInfo().getPath());
        try {
            // Valida el token
            Jwts.parser().setSigningKey(KeyUtils.KEY).parseClaimsJws(token);
            LOGGER.info("Token válido: " + token);
        } catch (Exception e) {
        	LOGGER.warn("Token inválido: " + token);
            requestContext.abortWith(Response.status(UNAUTHORIZED).entity(errorMessage).build());
            return;
        }
	}
}
