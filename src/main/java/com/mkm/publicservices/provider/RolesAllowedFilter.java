package com.mkm.publicservices.provider;

import static com.mkm.publicservices.util.KeyUtils.KEY;
import static javax.ws.rs.Priorities.AUTHENTICATION;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.glassfish.jersey.Severity.WARNING;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Priority;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mkm.publicservices.dao.UsuarioDao;
import com.mkm.publicservices.dao.impl.UsuarioDaoImpl;
import com.mkm.publicservices.model.TipoPerfil;
import com.mkm.publicservices.model.Usuario;
import com.mkm.publicservices.util.ErrorMessage;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Provider
@Priority(AUTHENTICATION)
public class RolesAllowedFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	private static final Logger LOGGER = LogManager.getLogger(RolesAllowedFilter.class);
	private String token = null;

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.container.ContainerRequestFilter
	 * #filter(javax.ws.rs.container.ContainerRequestContext)
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			// Get the HTTP Authorization header from the request
	        String authorizationHeader = requestContext.getHeaderString(AUTHORIZATION);
	        LOGGER.info("Encabezado de autentificación: " + authorizationHeader);
	        // Check if the HTTP Authorization header is present and formatted correctly
	        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
	        	LOGGER.warn("Encabezado de autentificación inválido: " + authorizationHeader);
	            throw new NotAuthorizedException("Debe tener un encabezado de autentificación");
	        }
	        // Extract the token from the HTTP Authorization header
	        token = authorizationHeader.substring("Bearer".length()).trim();
	        try {
	            // Validate the token
	            Claims claims = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token).getBody();
	            Set<String> hasSetRolesPermitidoUsuario = usuarioPerfiles(Integer.parseInt(claims.getId()), claims.getSubject());
	            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
	        	Set<String> hashSetRolesAnnotation = new HashSet<>(Arrays.asList(rolesAnnotation.value()));
	            for (String rolMetodo : hashSetRolesAnnotation) {
	            	if (hasSetRolesPermitidoUsuario.contains(rolMetodo)) {
	            		return;
	            	}
	           	}
	           	ErrorMessage errorMessage = new ErrorMessage();
	           	errorMessage.setErrorCode(FORBIDDEN);
	           	errorMessage.setErrorMessage("No tienes permiso para acceder a este recurso");
	           	errorMessage.setSeverity(WARNING);
	           	requestContext.abortWith(Response.status(FORBIDDEN).entity(errorMessage).build());
	           	return;
	          
	        } catch (Exception e) {
	        	LOGGER.warn("Token inválido: " + token);
	            requestContext.abortWith(Response.status(UNAUTHORIZED).build());
	        }
		}
	}
	
	private Set<String> usuarioPerfiles(int id, String nombreUsuario) {
		UsuarioDao usuarioDao = new UsuarioDaoImpl();
        Usuario usuario = usuarioDao.find(id);
        if(usuario != null && usuario.getNombreUsuario().equals(nombreUsuario)) {
        	Set<String> hasSetRolesPermitidoUsuario = new HashSet<>();
        	for(TipoPerfil tipoPerfil: usuario.getTipoPerfiles()) {
        		hasSetRolesPermitidoUsuario.add(tipoPerfil.getTipoPerfil());
        	}
        	return hasSetRolesPermitidoUsuario;
        } else {
        	LOGGER.warn("Token inválido: " + token);
        	throw new NotAuthorizedException("Authorization header must be provided");
        }
	}
}
