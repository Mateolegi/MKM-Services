package com.mkm.publicservices.provider;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class AllowCrossOrigin implements ContainerResponseFilter {

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.container.ContainerResponseFilter
	 * #filter(javax.ws.rs.container.ContainerRequestContext, javax.ws.rs.container.ContainerResponseContext)
	 */
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
		responseContext.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		responseContext.getHeaders().putSingle("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
		responseContext.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type");
	}
}
