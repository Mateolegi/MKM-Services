package com.mkm.publicservices.constant;

public class TipoParametro {

	public static final int ENERGIA = 1;
	public static final int ACUEDUCTO = 2;
	public static final int ALCANTARILLADO = 3;
	public static final int GAS = 4;

	private TipoParametro() {}
}
