package com.mkm.publicservices.constant;

public class TipoEstadoLiquidacion {

	public static final int GENERADO = 1;
	public static final int ENTREGADO = 2;
	public static final int PAGADO = 3;

	private TipoEstadoLiquidacion() {}
}
