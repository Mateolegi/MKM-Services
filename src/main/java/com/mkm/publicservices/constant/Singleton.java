package com.mkm.publicservices.constant;

import java.sql.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mkm.publicservices.util.DateDeserializer;

public class Singleton {

	public static final Gson GSON;

	static {
		GSON = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.setDateFormat("yyyy-MM-dd")
				.registerTypeAdapter(Date.class, new DateDeserializer())
				.create();
	}
}
