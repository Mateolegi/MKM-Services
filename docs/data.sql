CREATE DATABASE  IF NOT EXISTS `mkm` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mkm`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mkm
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignacion_subsidio`
--

DROP TABLE IF EXISTS `asignacion_subsidio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignacion_subsidio` (
  `ID_ASIGNACION_SUBSIDIO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_HOGAR` int(10) unsigned NOT NULL,
  `ID_CICLO_CONSUMO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_ASIGNACION_SUBSIDIO`),
  KEY `ID_HOGAR_idx` (`ID_HOGAR`),
  KEY `ID_CICLO_CONSUMO_ASIGNACION_SUBSIDIO_idx` (`ID_CICLO_CONSUMO`),
  CONSTRAINT `ID_CICLO_CONSUMO_ASIGNACION_SUBSIDIO` FOREIGN KEY (`ID_CICLO_CONSUMO`) REFERENCES `ciclo_consumo` (`ID_CICLO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_HOGAR_ASIGNACION_SUBSIDIO` FOREIGN KEY (`ID_HOGAR`) REFERENCES `hogar` (`ID_HOGAR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignacion_subsidio`
--

LOCK TABLES `asignacion_subsidio` WRITE;
/*!40000 ALTER TABLE `asignacion_subsidio` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignacion_subsidio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciclo_consumo`
--

DROP TABLE IF EXISTS `ciclo_consumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciclo_consumo` (
  `ID_CICLO_CONSUMO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_HOGAR` int(10) unsigned NOT NULL,
  `FECHA_INICIO` date NOT NULL,
  `FECHA_FIN` date NOT NULL,
  PRIMARY KEY (`ID_CICLO_CONSUMO`),
  KEY `ID_HOGAR_HOGAR_idx` (`ID_HOGAR`),
  CONSTRAINT `ID_HOGAR_HOGAR` FOREIGN KEY (`ID_HOGAR`) REFERENCES `hogar` (`ID_HOGAR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciclo_consumo`
--

LOCK TABLES `ciclo_consumo` WRITE;
/*!40000 ALTER TABLE `ciclo_consumo` DISABLE KEYS */;
INSERT INTO `ciclo_consumo` VALUES (1,1,'2018-01-01','2018-01-31'),(2,1,'2018-02-01','2018-02-28'),(3,1,'2018-03-01','2018-03-31'),(4,1,'2018-04-01','2018-03-30');
/*!40000 ALTER TABLE `ciclo_consumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumo`
--

DROP TABLE IF EXISTS `consumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumo` (
  `ID_CONSUMO` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'https://www.epm.com.co/site/portals/Descargas/cuentame/cuentame.pdf',
  `ID_TIPO_CONSUMO` int(10) unsigned NOT NULL,
  `CONSUMO` double NOT NULL,
  `ID_CICLO_CONSUMO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_CONSUMO`),
  KEY `ID_TIPO_CONSUMO_TIPO_CONSUMO_idx` (`ID_TIPO_CONSUMO`),
  KEY `ID_CICLO_CONSUMO_CICLO_CONSUMO_idx` (`ID_CICLO_CONSUMO`),
  CONSTRAINT `ID_CICLO_CONSUMO_CICLO_CONSUMO` FOREIGN KEY (`ID_CICLO_CONSUMO`) REFERENCES `ciclo_consumo` (`ID_CICLO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_TIPO_CONSUMO_TIPO_CONSUMO` FOREIGN KEY (`ID_TIPO_CONSUMO`) REFERENCES `tipo_consumo` (`ID_TIPO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumo`
--

LOCK TABLES `consumo` WRITE;
/*!40000 ALTER TABLE `consumo` DISABLE KEYS */;
INSERT INTO `consumo` VALUES (1,1,140,1),(2,2,20,1),(3,3,15,1),(4,4,15000,1),(5,1,73,2),(6,2,27,2),(7,3,15,2),(8,4,12600,2),(9,1,200,3),(10,2,33,3),(11,3,27,3),(12,4,19674,3),(13,1,73,4),(14,2,13,4),(15,3,13,4),(16,4,12679,4);
/*!40000 ALTER TABLE `consumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direccion` (
  `ID_DIRECCION` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'http://hechosdetransito.com/estructura-de-la-nomenclatura/',
  `ID_VIA_PRINCIPAL` int(10) unsigned NOT NULL,
  `NUMERO_VIA_PRINCIPAL` int(3) unsigned NOT NULL,
  `LETRA_PRINCIPAL` varchar(1) DEFAULT NULL,
  `LETRA_SUFIJO` varchar(1) DEFAULT NULL,
  `CUADRANTE_PRINCIPAL` varchar(1) DEFAULT NULL,
  `NUMERO_VIA_GENERADORA` int(3) unsigned NOT NULL,
  `LETRA_GENERADORA` varchar(1) DEFAULT NULL,
  `LETRA_SUFIJO_GENERADORA` varchar(1) DEFAULT NULL,
  `CUADRANTE_GENERADORA` varchar(1) DEFAULT NULL,
  `COMPLEMENTO` int(11) NOT NULL,
  `INTERIOR` varchar(20) DEFAULT NULL,
  `OBSERVACION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID_DIRECCION`),
  UNIQUE KEY `ID_DIRECCION_UNIQUE` (`ID_DIRECCION`),
  KEY `ID_VIA_PRINCIPAL_DIRECCION_idx` (`ID_VIA_PRINCIPAL`),
  CONSTRAINT `ID_VIA_PRINCIPAL_DIRECCION` FOREIGN KEY (`ID_VIA_PRINCIPAL`) REFERENCES `tipo_via` (`ID_TIPO_VIA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
INSERT INTO `direccion` VALUES (1,2,16,'B',NULL,NULL,32,NULL,NULL,NULL,50,'435',NULL);
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_liquidacion`
--

DROP TABLE IF EXISTS `estado_liquidacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_liquidacion` (
  `ID_ESTADO_LIQUIDACION` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_LIQUIDACION_CICLO_VIVIENDA` int(10) unsigned NOT NULL,
  `ID_TIPO_ESTADO_LIQUIDACION` int(10) unsigned NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_USUARIO_RESPONSABLE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_ESTADO_LIQUIDACION`),
  KEY `ID_LIQUIDACION_CICLO_VIVIENDA_ESTADO_LIQUIDACION_idx` (`ID_LIQUIDACION_CICLO_VIVIENDA`),
  KEY `ID_TIPO_ESTADO_LIQUIDACION_ESTADO_LIQUIDACION_idx` (`ID_TIPO_ESTADO_LIQUIDACION`),
  KEY `ID_USUARIO_RESPONSABLE_ESTADO_LIQUIDACION_idx` (`ID_USUARIO_RESPONSABLE`),
  CONSTRAINT `ID_LIQUIDACION_CICLO_VIVIENDA_ESTADO_LIQUIDACION` FOREIGN KEY (`ID_LIQUIDACION_CICLO_VIVIENDA`) REFERENCES `liquidacion_ciclo_vivienda` (`ID_LIQUIDACION_CICLO_VIVIENDA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_TIPO_ESTADO_LIQUIDACION_ESTADO_LIQUIDACION` FOREIGN KEY (`ID_TIPO_ESTADO_LIQUIDACION`) REFERENCES `tipo_estado_liquidacion` (`ID_TIPO_ESTADO_LIQUIDACION`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_USUARIO_RESPONSABLE_ESTADO_LIQUIDACION` FOREIGN KEY (`ID_USUARIO_RESPONSABLE`) REFERENCES `usuario` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_liquidacion`
--

LOCK TABLES `estado_liquidacion` WRITE;
/*!40000 ALTER TABLE `estado_liquidacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_liquidacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hogar`
--

DROP TABLE IF EXISTS `hogar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hogar` (
  `ID_HOGAR` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_DIRECCION` int(10) unsigned NOT NULL,
  `TELEFONO` varchar(20) NOT NULL,
  `ID_RESPONSABLE` int(10) unsigned NOT NULL,
  `ESTRATO_SOCIOECONOMICO` int(11) NOT NULL,
  `ID_USUARIO_REGISTRA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_HOGAR`),
  UNIQUE KEY `ID_HOGAR_UNIQUE` (`ID_HOGAR`),
  UNIQUE KEY `TELEFONO_UNIQUE` (`TELEFONO`),
  UNIQUE KEY `ID_DIRECCION_UNIQUE` (`ID_DIRECCION`),
  KEY `ID_RESPONSABLE_idx` (`ID_RESPONSABLE`),
  KEY `ID_USUARIO_REGISTRA_idx` (`ID_USUARIO_REGISTRA`),
  CONSTRAINT `ID_DIRECCION_HOGAR` FOREIGN KEY (`ID_DIRECCION`) REFERENCES `direccion` (`ID_DIRECCION`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_RESPONSABLE` FOREIGN KEY (`ID_RESPONSABLE`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_USUARIO_REGISTRA` FOREIGN KEY (`ID_USUARIO_REGISTRA`) REFERENCES `usuario` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hogar`
--

LOCK TABLES `hogar` WRITE;
/*!40000 ALTER TABLE `hogar` DISABLE KEYS */;
INSERT INTO `hogar` VALUES (1,1,'2934343',1,4,1);
/*!40000 ALTER TABLE `hogar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liquidacion_ciclo_vivienda`
--

DROP TABLE IF EXISTS `liquidacion_ciclo_vivienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liquidacion_ciclo_vivienda` (
  `ID_LIQUIDACION_CICLO_VIVIENDA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_HOGAR` int(10) unsigned NOT NULL,
  `ID_CICLO_CONSUMO` int(10) unsigned NOT NULL,
  `APLICA_SUBSIDIO` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID_LIQUIDACION_CICLO_VIVIENDA`),
  KEY `ID_HOGAR_LIQUIDACION_CICLO_VIVIENDA_idx` (`ID_HOGAR`),
  KEY `ID_CICLO_CONSUMO_LIQUIDACION_CICLO_VIVIENDA_idx` (`ID_CICLO_CONSUMO`),
  CONSTRAINT `ID_CICLO_CONSUMO_LIQUIDACION_CICLO_VIVIENDA` FOREIGN KEY (`ID_CICLO_CONSUMO`) REFERENCES `ciclo_consumo` (`ID_CICLO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_HOGAR_LIQUIDACION_CICLO_VIVIENDA` FOREIGN KEY (`ID_HOGAR`) REFERENCES `hogar` (`ID_HOGAR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liquidacion_ciclo_vivienda`
--

LOCK TABLES `liquidacion_ciclo_vivienda` WRITE;
/*!40000 ALTER TABLE `liquidacion_ciclo_vivienda` DISABLE KEYS */;
/*!40000 ALTER TABLE `liquidacion_ciclo_vivienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liquidacion_valor_consumo`
--

DROP TABLE IF EXISTS `liquidacion_valor_consumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liquidacion_valor_consumo` (
  `ID_LIQUIDACION_VALOR_CONSUMO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_CONSUMO` int(10) unsigned NOT NULL,
  `VALOR_CONSUMO` decimal(12,4) NOT NULL,
  PRIMARY KEY (`ID_LIQUIDACION_VALOR_CONSUMO`),
  KEY `ID_TIPO_CONSUMO_LIQUIDACION_VALOR_CONSUMO_idx` (`ID_TIPO_CONSUMO`),
  CONSTRAINT `ID_TIPO_CONSUMO_LIQUIDACION_VALOR_CONSUMO` FOREIGN KEY (`ID_TIPO_CONSUMO`) REFERENCES `tipo_consumo` (`ID_TIPO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liquidacion_valor_consumo`
--

LOCK TABLES `liquidacion_valor_consumo` WRITE;
/*!40000 ALTER TABLE `liquidacion_valor_consumo` DISABLE KEYS */;
/*!40000 ALTER TABLE `liquidacion_valor_consumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `ID_PERSONA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_DOCUMENTO` int(10) unsigned NOT NULL,
  `NUMERO_DOCUMENTO` varchar(20) NOT NULL,
  `NOMBRE` varchar(45) NOT NULL,
  `APELLIDO` varchar(45) NOT NULL,
  `FECHA_NACIMIENTO` date NOT NULL,
  `EMAIL` varchar(60) DEFAULT NULL,
  `GENERO` tinyint(3) unsigned NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_PERSONA`),
  UNIQUE KEY `NUMERO_DOCUMENTO_UNIQUE` (`NUMERO_DOCUMENTO`),
  UNIQUE KEY `EMAIL_UNIQUE` (`EMAIL`),
  KEY `ID_TIPO_DOCUMENTO_PERSONA_idx` (`ID_TIPO_DOCUMENTO`),
  CONSTRAINT `ID_TIPO_DOCUMENTO_PERSONA` FOREIGN KEY (`ID_TIPO_DOCUMENTO`) REFERENCES `tipo_documento` (`ID_TIPO_DOCUMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,1,'1238938246','Mateo','Leal','1999-12-15','mateolegi@gmail.com',0,'2018-04-09 02:44:01');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promedio_consumo_ciclo`
--

DROP TABLE IF EXISTS `promedio_consumo_ciclo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promedio_consumo_ciclo` (
  `ID_PROMEDIO_CONSUMO_CICLO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_CONSUMO` int(10) unsigned NOT NULL,
  `PROMEDIO` double NOT NULL,
  `ID_ASIGNACION_SUBSIDIO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_PROMEDIO_CONSUMO_CICLO`),
  KEY `ID_TIPO_CONSUMO_TIPO_CONSUMO_idx` (`ID_TIPO_CONSUMO`),
  KEY `ID_ASIGNACION_CONSUMO_idx` (`ID_ASIGNACION_SUBSIDIO`),
  CONSTRAINT `ID_ASIGNACION_CONSUMO_PROMEDIO_CONSUMO_CICLO` FOREIGN KEY (`ID_ASIGNACION_SUBSIDIO`) REFERENCES `asignacion_subsidio` (`ID_ASIGNACION_SUBSIDIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_TIPO_CONSUMO_PROMEDIO_CONSUMO_CICLO` FOREIGN KEY (`ID_TIPO_CONSUMO`) REFERENCES `tipo_consumo` (`ID_TIPO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promedio_consumo_ciclo`
--

LOCK TABLES `promedio_consumo_ciclo` WRITE;
/*!40000 ALTER TABLE `promedio_consumo_ciclo` DISABLE KEYS */;
/*!40000 ALTER TABLE `promedio_consumo_ciclo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_consumo`
--

DROP TABLE IF EXISTS `tipo_consumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_consumo` (
  `ID_TIPO_CONSUMO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_CONSUMO` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_TIPO_CONSUMO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_consumo`
--

LOCK TABLES `tipo_consumo` WRITE;
/*!40000 ALTER TABLE `tipo_consumo` DISABLE KEYS */;
INSERT INTO `tipo_consumo` VALUES (1,'Energía'),(2,'Acueducto'),(3,'Alcantarillado'),(4,'Gas');
/*!40000 ALTER TABLE `tipo_consumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `ID_TIPO_DOCUMENTO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_DOCUMENTO` varchar(45) NOT NULL,
  `ABREVIACION` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_TIPO_DOCUMENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` VALUES (1,'Cédula de ciudadanía','CC'),(2,'Cédula de extranjería','CE');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_liquidacion`
--

DROP TABLE IF EXISTS `tipo_estado_liquidacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_liquidacion` (
  `ID_TIPO_ESTADO_LIQUIDACION` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_ESTADO_LIQUIDACION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_TIPO_ESTADO_LIQUIDACION`),
  UNIQUE KEY `TIPO_ESTADO_LIQUIDACION_UNIQUE` (`TIPO_ESTADO_LIQUIDACION`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_liquidacion`
--

LOCK TABLES `tipo_estado_liquidacion` WRITE;
/*!40000 ALTER TABLE `tipo_estado_liquidacion` DISABLE KEYS */;
INSERT INTO `tipo_estado_liquidacion` VALUES (2,'Entregado'),(1,'Generado'),(3,'Pagado');
/*!40000 ALTER TABLE `tipo_estado_liquidacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_parametro`
--

DROP TABLE IF EXISTS `tipo_parametro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_parametro` (
  `ID_TIPO_PARAMETRO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_PARAMETRO` varchar(45) NOT NULL,
  `VALOR_PARAMETRO` varchar(200) NOT NULL,
  `DESCRIPCION` varchar(300) NOT NULL,
  PRIMARY KEY (`ID_TIPO_PARAMETRO`),
  UNIQUE KEY `TIPO_PARAMETRO_UNIQUE` (`TIPO_PARAMETRO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_parametro`
--

LOCK TABLES `tipo_parametro` WRITE;
/*!40000 ALTER TABLE `tipo_parametro` DISABLE KEYS */;
INSERT INTO `tipo_parametro` VALUES (1,'VALOR-SUBSIDIO-ENERGIA','12300','Representa el valor base para aplicar el subsidio de energía'),(2,'VALOR-SUBSIDIO-ACUEDUCTO','12500','Representa el valor base para aplicar el subsidio de acueductos'),(3,'VALOR-SUBSIDIO-ALCANTARILLADO','12500','Representa el valor base para aplicar el subsidio de alcantarillado'),(4,'VALOR-SUBSIDIO-GAS','15800','Representa el valor base para aplicar el subsidio de gas');
/*!40000 ALTER TABLE `tipo_parametro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_perfil`
--

DROP TABLE IF EXISTS `tipo_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_perfil` (
  `ID_TIPO_PERFIL` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_PERFIL` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_TIPO_PERFIL`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_perfil`
--

LOCK TABLES `tipo_perfil` WRITE;
/*!40000 ALTER TABLE `tipo_perfil` DISABLE KEYS */;
INSERT INTO `tipo_perfil` VALUES (1,'Administración'),(2,'Operador');
/*!40000 ALTER TABLE `tipo_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_via`
--

DROP TABLE IF EXISTS `tipo_via`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_via` (
  `ID_TIPO_VIA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_VIA` varchar(45) NOT NULL,
  `ABREVIACION` varchar(5) NOT NULL,
  PRIMARY KEY (`ID_TIPO_VIA`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_via`
--

LOCK TABLES `tipo_via` WRITE;
/*!40000 ALTER TABLE `tipo_via` DISABLE KEYS */;
INSERT INTO `tipo_via` VALUES (1,'Calle','Cl'),(2,'Carrera','Cr'),(3,'Transversal','Tv'),(4,'Diagonal','Dg'),(5,'Avenida','Av'),(6,'Circular','Cq'),(7,'Pasaje','Pj'),(8,'Kilómetro','Km'),(9,'Vía','Vi');
/*!40000 ALTER TABLE `tipo_via` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID_USUARIO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_PERSONA` int(10) unsigned NOT NULL,
  `NOMBRE_USUARIO` varchar(20) NOT NULL,
  `CONTRASENA` varchar(300) NOT NULL,
  `ESTADO` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `FECHA_CREACION` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_USUARIO`),
  UNIQUE KEY `NOMBRE_USUARIO_UNIQUE` (`NOMBRE_USUARIO`),
  UNIQUE KEY `ID_PERSONA_UNIQUE` (`ID_PERSONA`),
  CONSTRAINT `ID_PERSONA_USUARIO` FOREIGN KEY (`ID_PERSONA`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'mateolegi','admin',1,'2018-04-19 05:16:28');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_perfil`
--

DROP TABLE IF EXISTS `usuario_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_perfil` (
  `ID_USUARIO` int(10) unsigned NOT NULL,
  `ID_TIPO_PERFIL` int(10) unsigned NOT NULL,
  KEY `ID_USUARIO_USUARIO_PERFIL_idx` (`ID_USUARIO`),
  KEY `ID_TIPO_PERFIL_USUARIO_PERFIL_idx` (`ID_TIPO_PERFIL`),
  CONSTRAINT `ID_TIPO_PERFIL_USUARIO_PERFIL` FOREIGN KEY (`ID_TIPO_PERFIL`) REFERENCES `tipo_perfil` (`ID_TIPO_PERFIL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_USUARIO_USUARIO_PERFIL` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_perfil`
--

LOCK TABLES `usuario_perfil` WRITE;
/*!40000 ALTER TABLE `usuario_perfil` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valor_consumo`
--

DROP TABLE IF EXISTS `valor_consumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valor_consumo` (
  `ID_VALOR_CONSUMO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_CONSUMO` int(10) unsigned NOT NULL,
  `VALOR_CONSUMO` decimal(12,4) NOT NULL,
  `ANNO` year(4) NOT NULL,
  PRIMARY KEY (`ID_VALOR_CONSUMO`),
  KEY `ID_TIPO_CONSUMO_VALOR_CONSUMO_idx` (`ID_TIPO_CONSUMO`),
  CONSTRAINT `ID_TIPO_CONSUMO_VALOR_CONSUMO` FOREIGN KEY (`ID_TIPO_CONSUMO`) REFERENCES `tipo_consumo` (`ID_TIPO_CONSUMO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valor_consumo`
--

LOCK TABLES `valor_consumo` WRITE;
/*!40000 ALTER TABLE `valor_consumo` DISABLE KEYS */;
INSERT INTO `valor_consumo` VALUES (1,1,168.1643,2018),(2,2,959.6153,2018),(3,3,959.6153,2018),(4,4,1.2398,2018);
/*!40000 ALTER TABLE `valor_consumo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-19  8:39:42
